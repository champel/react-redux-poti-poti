webpackConfig = require('./webpack.config')

module.exports = function(config) {
    config.set({
        frameworks: ['mocha', 'chai'],
        files: [
            'karma.index.js'
        ],
        browsers : ['Chrome'],
        preprocessors: {
            'karma.index.js': [ 'webpack', 'sourcemap' ], 
        },
        webpack: {
            mode: 'development',
            devtool: 'inline-source-map',
            module: webpackConfig.module,
            resolve: webpackConfig.resolve
        },
        webpackMiddleware: {
            quiet: true,
            stats: {
                colors: true
            }
        },
        reporters: ['mocha'],
        mime: {
            'text/x-typescript': ['ts','tsx']
        },
        mochaReporter: {
            showDiff: true
        },
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
    });
  };