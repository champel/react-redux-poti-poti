describe('home page', function() {
    const muiTextField = {
        input: '> div > input'
    }
    it('!', function() {
        const firstName = 'Paco'
        cy.visit('http://localhost:8080')
        cy.get('[data-testid="cardGreeting"] [data-testid="firstname"]').within(() => {
            cy.get(muiTextField.input)
                .type(firstName)
        })
        cy.get('[data-testid="cardGreeting"] [data-testid="lastname"] input')
            .type("Perez")
        cy.get('[data-testid="cardGreeting"] [data-testid="say-hello"]')
            .click()

        cy.get('.percentages-table > .statistic:nth-child(3) > .firstname')
            .should('to.contain', firstName)
        cy.get('.percentages-table > .statistic:nth-child(3) > .percentage')
            .should('to.contain', '25%')
    })
  })