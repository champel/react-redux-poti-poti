import { connect } from 'react-redux'
import { compose, lifecycle, mapProps } from 'recompose'
import { createStructuredSelector } from 'reselect';

export default (isLoading, isLoaded, requestAll) => compose(
    connect(createStructuredSelector({
        isLoading,
        isLoaded,
        initialProps: (_, ownProps) => ownProps})),
    lifecycle({
        componentDidMount() {
            if (!this.props['isLoaded'] && !this.props['isLoading']) {
                this.props['dispatch'](requestAll())
            }
        }
    }),
    mapProps(({initialProps}) => initialProps)
)
