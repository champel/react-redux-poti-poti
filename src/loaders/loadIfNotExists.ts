import { connect } from 'react-redux'
import { compose, lifecycle, mapProps } from 'recompose'
import { createStructuredSelector } from 'reselect';

export default (getId, getOne, requestOne) => compose(
    connect(createStructuredSelector({
        id: getId,
        entity: getOne(getId),
        initialProps: (_, ownProps) => ownProps, 
    })),
    lifecycle({
        componentDidMount() {
            if (!this.props['entity']) {
                this.props['dispatch'](requestOne(this.props['id']))
            }
        }
    }),
    mapProps((props) => props['initialProps'])
)
