import * as React from 'react'
import * as PropTypes from 'prop-types'
import { SFC } from 'react';

export interface FormProps {
    onSubmit: (elements: object) => void
}

export const Form: SFC<FormProps> = ({children, onSubmit}) => (
    <form action="#" onSubmit={e => { e.preventDefault(); onSubmit(e.target['elements']); }}>
        {children}
    </form>
)

Form.propTypes = {
    onSubmit: PropTypes.func.isRequired
}

export default Form;