import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import createHistory from 'history/createBrowserHistory'

import { CssBaseline } from '@material-ui/core';

import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'

import { getStateWith } from 'reselect-tools'


import { App, reducer, sagas } from './app'

import 'typeface-roboto'
import './i18n'

const history = createHistory();
const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;

const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(sagaMiddleware, routerMiddleware(history))
));

getStateWith(() => store.getState())

sagas.forEach(saga => sagaMiddleware.run(saga));

const render = Component => {
  ReactDOM.render(
      <AppContainer>
        <CssBaseline>
          <Provider store={store}>
            <ConnectedRouter history={history}>
              <Component/>
            </ConnectedRouter>
          </Provider>
        </CssBaseline>
      </AppContainer>,
    document.getElementById('app')
  )
}

render(App)

if (module['hot']) {
  module['hot'].accept('./app', () => render(App))
}