import * as React from 'react'

import Typography from '@material-ui/core/Typography'

const About = () => (
    <Typography variant="display1">Made for testing!</Typography>
);

export default About;