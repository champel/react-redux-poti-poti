import { sagas as hello } from './hello'
import { sagas as devices } from './devices'
import { sagas as tags } from './tags'
import sessionStatistics from './session-statistics/sagas'
import layout from './layout/sagas'

export default [
    ...sessionStatistics,
    ...hello,
    ...devices,
    ...tags,
    ...layout
];