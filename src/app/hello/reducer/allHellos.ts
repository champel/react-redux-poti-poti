import { SAY_HELLO } from '../constants'

const allHellos = (state = {}, action) => {
  switch (action.type) {
    case SAY_HELLO:
      const { firstname, lastname } = action
      const key = `${firstname}-${lastname}`
      // Destructuring to ensure the key is the first and not overwritten
      const { [key]: existingValue, ...rest } = state
      return {
        [key]: { count: existingValue ? existingValue.count + 1 : 1, firstname, lastname },
        ...rest,
      };
    default:
      return state
  }
}
export default allHellos
