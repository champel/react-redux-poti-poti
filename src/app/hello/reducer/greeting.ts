import { SET_GREETING } from '../constants'

const greeting = (state = "", action) => {
    switch (action.type) {
      case SET_GREETING:
        return action.greeting;
      default:
        return state;
    }
  }
  
export default greeting
