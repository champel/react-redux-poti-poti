import { SAY_HELLO } from '../constants'

function registerHello(state, action) {
    const { firstname, lastname } = action

    const statistic = state[firstname]

    const increment = (firstname == 'Júlia' && lastname == 'Champel') ? 100000 : 1;

    return {
        ...state,
        [firstname]: statistic == null ? increment : statistic+increment
    }
}

function firstnameStatistics(state = { "Gandalf": 1, "Aragorn": 2}, action) {
    switch(action.type) {
        case SAY_HELLO: return registerHello(state, action)
        default: return state
    }
}

export default firstnameStatistics
