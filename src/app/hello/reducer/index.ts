import { combineReducers } from 'redux'

import allHellos from './allHellos'
import greeting from './greeting'
import firstnameStatistics from './firstnameStatistics'
import lastHello from './lastHello'

export default combineReducers({
    allHellos,
    greeting,
    firstnameStatistics,
    lastHello
})