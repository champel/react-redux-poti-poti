import { expect } from 'chai'

import reducer from './greeting'
import { SET_GREETING } from '../constants'

describe('hello', () => {
  describe('3. reducer', () => {
    describe('greetings reducer', () => {
      it('should return an empty string as initial state', () => {
        expect(reducer(undefined, {})).eql("")
      })

      it('should handle "SET_GREETING" storing the last "greeting"', () => {
        const greeting = "greeting"
        expect(
          reducer("", {
            type: SET_GREETING,
            greeting
          })
        ).to.deep.equal(greeting)
      })
    })

    describe('selectors', () => {
    })
  })
})