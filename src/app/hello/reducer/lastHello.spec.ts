import { expect } from 'chai'

import reducer from './lastHello'
import { SAY_HELLO } from '../constants'

describe('hello', () => {
  describe('3. reducer', () => {
    describe('last hello reducer', () => {
      it('should return an empty object as initial state', () => {
        expect(reducer(undefined, {})).eql({})
      })

      it('should handle "SAY_HELLO" storing the "firstname" and "lastname" as last submitted', () => {
        const firstname = "fisrtname"
        const lastname = "lastname"
        expect(
          reducer({}, {
            type: SAY_HELLO,
            firstname,
            lastname
          })
        ).to.deep.equal(
          {
              firstname,
              lastname
          }
        )
      })
    })
  })
})