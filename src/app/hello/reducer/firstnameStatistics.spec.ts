import { expect } from 'chai'

import reducer from './firstnameStatistics'
import { SAY_HELLO } from '../constants'

describe('hello', () => {
  describe('3. reducer', () => {
    describe('firstnameStatistics reducer', () => {
      it('should return { "Gandalf": 1, "Aragorn": 2 } as initial state', () => {
        expect(reducer(undefined, {})).eql({ 'Gandalf': 1, 'Aragorn': 2 })
      })

      it('should handle "SAY_HELLO" creating an entry with the "firstname" and value 1 if does not exist', () => {
        const firstname = "fisrtname"
        const lastname = "lastname"
        expect(
          reducer({} as any, {
            type: SAY_HELLO,
            firstname,
            lastname
          })
        ).to.deep.equal(
          {
              [firstname]: 1
          }
        )
      })

      it('should handle "SAY_HELLO" incrementing the value if the "firstname" already exists', () => {
        const firstname = "firstname"
        const lastname = "lastname"
        expect(
          reducer({ firstname: 1} as any, {
            type: SAY_HELLO,
            firstname,
            lastname
          })
        ).to.deep.equal(
          {
              [firstname]: 2
          }
        )
      })
    })
  })
})