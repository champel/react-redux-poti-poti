import { SAY_HELLO } from '../constants'

const hello = (state = {}, action) => {
    switch (action.type) {
      case SAY_HELLO:
        return {
            ...state,
            firstname: action.firstname,
            lastname: action.lastname
          };
      default:
        return state
    }
  }
  
export default hello
