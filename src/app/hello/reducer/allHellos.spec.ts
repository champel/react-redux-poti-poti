import { expect } from 'chai'

import reducer from  './allHellos'
import { SAY_HELLO } from '../constants'

describe('hello', () => {
  describe('3. reducer', () => {
    const firstname = 'firstname'
    const lastname = 'lastname'
    const helloAction = {
      type: SAY_HELLO,
      firstname,
      lastname
    }

    const initialEntry = {
      count: 1,
      firstname: 'initialFirstname',
      lastname: 'initialLastname'
    }
    const initialKey = 'initialFirstname-initialLastName'

    describe('allHellos reducer', () => {
      it('should return an empty object as initial state', () => {
        expect(reducer(undefined, {})).eql({})
      })

      it('should handle "SAY_HELLO" on an empty state registering the combination of "firstname" and "lastname" with a counter of 1', () => {
        const state = reducer({}, helloAction)
        expect(state).to.deep.equal(
          { 
            ['firstname-lastname']: {
              count: 1,
              firstname,
              lastname
            }
          }
        )
      })

      it('should handle "SAY_HELL" in a non empty state registering a new combination of "firstname" and "lastname" with a counter of 1 putting it as the first key', () => {
        const state = reducer({ [initialKey]: initialEntry }, helloAction)
        expect(state).to.deep.equal(
          { 
            ['firstname-lastname']: {
              count: 1,
              firstname,
              lastname
            },
            [initialKey]: initialEntry
          }
        )
        expect(Object.keys(state)).to.deep.equal(
          [ 'firstname-lastname', initialKey ]
        )
      })
      it('should handle "SAY_HELLO" of an existing combination of "firstname" and "lastname" incrementing the counter and putting it as the first key', () => {
        expect(
          reducer({
            [initialKey]: initialEntry,
            ['firstname-lastname']: {
              count: 1,
              firstname,
              lastname
            }}, helloAction)
        ).to.deep.equal(
          { 
            ['firstname-lastname']: {
              count: 2,
              firstname,
              lastname
            },
            [initialKey]: initialEntry
          }
        )
      })
    })
  })
})