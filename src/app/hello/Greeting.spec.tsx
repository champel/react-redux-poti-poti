import * as React from 'react'
import { mount } from 'enzyme'
import { spy, stub } from 'sinon'
import { expect } from 'chai'
import * as renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';

import * as selectors from './selectors'

import { createSink } from 'recompose';
import { Provider } from 'react-redux';
import { Greeting, enhance, GreetingProps } from './Greeting';

const sel = id => `[data-testid="${id}"]`
const typography = wrapper => wrapper.find('Typography')
const greetingTypography = wrapper => typography(wrapper.find(sel('greeting')))
const waitingTypography = wrapper => typography(wrapper.find(sel('waiting')))

const mockStore = configureStore([]);

describe('hello', () => {
    describe('5. components & containers', () => {
        describe('Greeting component', () => {
            it('should show greeging if defined', () => {
                const greeting = "greeting"
                const wrapper = mount(<Greeting greeting={greeting}/>);
                expect(greetingTypography(wrapper).contains(greeting))
            })

            it('should show "waiting someone..." if not defined', () => {
                const wrapper = mount(<Greeting greeting={undefined}/>);
                expect(waitingTypography(wrapper).contains("Waiting for someone..."))
            })
        })

        describe('Greeting enhancer', () => {
            it('should provide stored greeting', () => {
                const greeting = "greeting"
                const getGreetingStub = stub(selectors, 'getGreeting').returns(greeting)

                const Sink = createSink((props: GreetingProps) => {
                    expect(props).to.deep.include({
                        greeting
                    })
                })
                const EnhancedSink = enhance(Sink)

                const store = mockStore()
                renderer.create(
                    <Provider store={store}>
                        <EnhancedSink/>
                    </Provider>
                )
                getGreetingStub.restore()
            })
        })
    })
})