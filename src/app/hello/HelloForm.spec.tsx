import * as React from 'react'
import { mount } from 'enzyme'
import { spy, stub } from 'sinon'
import { expect } from 'chai'
import * as renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';

import * as selectors from './selectors'

import { HelloForm, HelloFormProps, enhance } from './HelloForm';
import { createSink } from 'recompose';
import { Provider } from 'react-redux';
import { sayHello } from './actions';

const sel = id => `[data-testid="${id}"]`
const textField = wrapper => wrapper.find('TextField')
const firstnameTextField = wrapper => textField(wrapper.find(sel('firstname')))
const lastnameTextField = wrapper => textField(wrapper.find(sel('lastname')))

const mockStore = configureStore([]);

describe('hello', () => {
  describe('5. components & containers', () => {
    const firstname = "Paco"
    const lastname = "Perez"
    describe('HelloForm component', () => {
      it('should show the last submitted firstname and lastname on the form', () => {
        const wrapper = mount(<HelloForm firstname={firstname} lastname={lastname} onSayHelloSubmit={undefined}/>);
        expect(firstnameTextField(wrapper).props().defaultValue).equals(firstname)
        expect(lastnameTextField(wrapper).props().defaultValue).equals(lastname)
      })

      it('should call onSayHelloSubmit with firstname and lastname on the form submission', () => {
        const onSayHelloSubmit = spy();
        const wrapper = mount(<HelloForm firstname={firstname} lastname={lastname} onSayHelloSubmit={onSayHelloSubmit}/>);
        wrapper.find('form').simulate('submit')
        expect(onSayHelloSubmit.withArgs(firstname,lastname).calledOnce)
      })
    })

    describe('HelloForm enhancer', () => {

      it('should provide last firstname and lastname', () => {
        const firstname = "Gandalf"
        const lastname = "Gonzalez"
        const getFirstnameStub = stub(selectors, 'getFirstname').returns(firstname)
        const getLastnameStub = stub(selectors, 'getLastname').returns(lastname)

        const Sink = createSink((props: HelloFormProps) => {
          expect(props).to.deep.include({
            firstname,
            lastname
          })
        })

        const EnhancedSink = enhance(Sink)

        const store = mockStore()
        renderer.create(
          <Provider store={store}>
            <EnhancedSink/>
          </Provider>
        )
        getFirstnameStub.restore()
        getLastnameStub.restore()
      })

      it('should provide a callback onSayHelloSubmit that dispatches a sayHello action with provided firstname and lastname', () => {
        const getFirstnameStub = stub(selectors, 'getFirstname').returns(firstname)
        const getLastnameStub = stub(selectors, 'getLastname').returns(lastname)

        const submitFirstname = "Bilbo"
        const submitLastname = "Bolson"
        const Sink = createSink((props: HelloFormProps) => {
          props.onSayHelloSubmit(submitFirstname,submitLastname)
        })

        const EnhancedSink = enhance(Sink)

        const store = mockStore()
        renderer.create(
          <Provider store={store}>
            <EnhancedSink/>
          </Provider>
        )
        expect(store.getActions()).to.deep.include(sayHello(submitFirstname, submitLastname))

        getFirstnameStub.restore()
        getLastnameStub.restore()
      })
    })
  })
})