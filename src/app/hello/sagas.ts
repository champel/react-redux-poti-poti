import { put, takeEvery } from 'redux-saga/effects'

import { SAY_HELLO } from './constants'
import { setGreeting } from './actions'

import { greetingCalculator } from './services'

function* calculateGreeting(action) {
  yield put(setGreeting(greetingCalculator(action['firstname'], action['lastname'])))
}

export function* greeter() {
  yield takeEvery(SAY_HELLO, calculateGreeting);
}

export default [ greeter ]