import * as React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';

import { Form } from '@/html'
import { Button } from '@material-ui/core';
import { TextField } from '@material-ui/core';

import { getFirstname, getLastname } from './selectors'
import { sayHello } from './actions'

export interface HelloFormProps {
    firstname: string
    lastname: string
    onSayHelloSubmit: (firstname: string, lastname: string) => void
}

export const HelloForm: React.SFC<HelloFormProps> = ({firstname, lastname, onSayHelloSubmit}) =>
    <Form onSubmit={elements => onSayHelloSubmit(elements['firstname'].value, elements['lastname'].value)}>
        <TextField name="firstname" label="First name" defaultValue={firstname} data-testid='firstname'/>
        &nbsp;
        <TextField name="lastname" label="Last name" defaultValue={lastname}  data-testid='lastname'/>
        <br/>
        <Button variant="contained" color="primary" type="submit" style={{marginTop: 10}} data-testid='say-hello'>
            Say hello
        </Button>
    </Form>

export const enhance = (Component: React.ComponentType) => {
    const mapStateToProps = createStructuredSelector({
        firstname: getFirstname,
        lastname: getLastname,
      })
      
    const mapDispatchToProps = {
        onSayHelloSubmit: sayHello
    }
    return connect(mapStateToProps,mapDispatchToProps)(Component)
}

export default enhance(HelloForm)