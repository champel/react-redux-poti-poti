import * as React from 'react'
import { mount, ReactWrapper } from 'enzyme'
import { stub } from 'sinon'
import { expect } from 'chai'
import * as renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';

import * as selectors from './selectors'

import { enhance, FirstnameCounts, FirstnameCountsProps } from './FirstnameCounts';
import { createSink } from 'recompose';
import { Provider } from 'react-redux';

const barChart = (wrapper: ReactWrapper) => wrapper.find('BarChart')

const mockStore = configureStore([]);

describe('hello', () => {
  describe('5. components & containers', () => {
    const data = {
        a: 1,
        b: 2
    }

    describe('FirstnameCounts component', () => {
      it('should show a barchart with a bar for each provided firstname', () => {
        const wrapper = mount(<FirstnameCounts data={data}/>);
        console.log(barChart(wrapper).debug())
        expect(barChart(wrapper).props().data).deep.equal([ data ])
      })
    })

    describe('FirstnameCounts enhancer', () => {

      it('should provide top first names', () => {
        const getTopFirstNamesStub = stub(selectors, 'getTopFirstNames').returns(data)

        const Sink = createSink((props: FirstnameCountsProps) => {
          expect(props).to.deep.include({
            data
          })
        })

        const EnhancedSink = enhance(Sink)

        const store = mockStore()
        renderer.create(
          <Provider store={store}>
            <EnhancedSink/>
          </Provider>
        )
        getTopFirstNamesStub.restore()
      })
    })
  })
})