import * as React from 'react'
import { connect } from 'react-redux'
import { ResponsiveHalfDonut } from '@/d3c'
import { Typography } from '@material-ui/core';

import { getValues, getPercentagesTable } from './selectors';

const PercentagesTable = ({data, width, maxWidth, colors}) =>
    <table style={{width: width, margin:'auto', maxWidth: maxWidth}}>
        <tbody className="percentages-table">
            {data.map(([key, value], i) => <tr key={key} className="statistic">
                <td className="firstname">
                    <Typography>
                        <svg width="20" height="12">
                            <rect width="12" height="12" style={{fill:colors[i]}} />
                        </svg>
                        {key ? key : "<no name>"}
                    </Typography>
                </td>
                <td style={{textAlign:"right"}} className="percentage">
                    <Typography>
                        {value}%
                    </Typography>
                </td>
            </tr>)}
        </tbody>
    </table>

export interface FirstnameStatisticsProps {
    values: number[],
    percentagesTable: object[],
    colors?: string[]
}

export const FirstnameStatistics: React.SFC<FirstnameStatisticsProps> = ({values, percentagesTable, colors = ["#0bd318", "#ffdb4c", "#ff8800", "#ff323e", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"]}) => (
    <div>
        {!values.length ?
            <p>Start to say hello to see the statistics</p> :
            <div>
                <ResponsiveHalfDonut data={values} height={200} colors={colors} width={200}/>
                <PercentagesTable data={percentagesTable} width="90%" maxWidth={380} colors={colors} />
            </div>
        }
    </div>)

export const enhance = (Component) => {
    const mapStateToProps = (state) => ({
        values: getValues(state),
        percentagesTable: getPercentagesTable(state)
    })
      
    const mapDispatchToProps = {
    }
      
    return connect(mapStateToProps, mapDispatchToProps)(Component)
}

export default enhance(FirstnameStatistics)