import { expect } from 'chai'

import { SET_GREETING, SAY_HELLO } from './constants'
import { setGreeting, sayHello } from './actions'

describe('hello', () => {
    describe('1. actions', () => {
        it('should handle sayHello generating a "SAY_HELLO" with "firstname" and "lastname"', () => {
            const firstname = 'firstname'
            const lastname = 'lastname'
            const expectedAction = {
                type: SAY_HELLO,
                firstname,
                lastname
            }
            expect(sayHello(firstname, lastname)).eql(expectedAction)
        })

        it('should handle setGreeting generating a "SET_GREETING" with a "greeting"', () => {
            const greeting = 'greeting'
            const expectedAction = {
                type: SET_GREETING,
                greeting
            }
            expect(setGreeting('greeting')).eql(expectedAction)
        })
    })
})