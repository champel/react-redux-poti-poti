import { expectSaga } from "redux-saga-test-plan"
import { stub } from "sinon"

import { greeter } from './sagas'

import * as services from './services'
import { sayHello, setGreeting } from './actions';

describe('hello', () => {
    describe('2. sagas', () => {
        it('should take every "SAY_HELLO" dispatching a "setGreeting" using "greetingCalculator" service ', () => {
            const firstname = "Gandalf"
            const lastname = "The grey"
            const greeting = firstname + " " + lastname

            const greetingCalculatorStub = stub(services, 'greetingCalculator')
            greetingCalculatorStub.withArgs(firstname,lastname).returns(greeting)

            return expectSaga(greeter)
                .put(setGreeting(greeting))
                .dispatch(sayHello(firstname,lastname))
                .silentRun()
                .then(() => {
                    greetingCalculatorStub.restore()
                })
        })
    })
})
  