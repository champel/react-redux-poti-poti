const greetingCalculator = (firstname, lastname) => {
    var greeting = 'Hello';
    if (firstname || lastname) {
        greeting += ', ';
        if (firstname && lastname) {
          greeting += firstname + ' ' + lastname;
        } else if (lastname) {
          greeting += 'Mx. ' + lastname;
        } else {
          greeting += firstname;
        }
    }
    greeting += '!';
    return greeting;
  }

export default greetingCalculator