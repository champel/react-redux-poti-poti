import key from './key'

export const SAY_HELLO = `@${key}/SAY_HELLO`
export const SET_GREETING = `@${key}/SET_GREETING`
