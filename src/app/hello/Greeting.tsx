import * as React from 'react'
import { SFC } from 'react'
import { connect } from 'react-redux'
import { Typography } from '@material-ui/core';

import { getGreeting } from './selectors'

export interface GreetingProps {
    greeting: string
}

export const Greeting: SFC<GreetingProps> = ({greeting}) => 
    <div>
        {greeting ?
            <Typography variant="display2" data-testid="greeting">{greeting}</Typography> :
            <Typography data-testid="waiting">Waiting for someone...</Typography>
        }
    </div>

export const enhance = (Component: React.ComponentType) => {
    const mapStateToProps = (state) => ({
        greeting: getGreeting(state),
    })
    return connect(mapStateToProps)(Component)
}

export default enhance(Greeting)