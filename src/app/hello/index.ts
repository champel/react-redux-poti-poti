import key from './key'
import HelloForm from './HelloForm'
import FirstnameCounts from './FirstnameCounts'
import FirstnameStatistics from './FirstnameStatistics'
import Greeting from './Greeting'

import reducer from './reducer'
import sagas from './sagas'

export { key, reducer, sagas, HelloForm, FirstnameStatistics, FirstnameCounts, Greeting }





//Hack to register selectors for reselect-tools
import { registerSelectors } from 'reselect-tools'
import * as selectors from './selectors.js'
registerSelectors(selectors)
