import { SET_GREETING, SAY_HELLO } from './constants'

export const sayHello = (firstname, lastname) => ({
    type: SAY_HELLO,
    firstname,
    lastname
})
  
export const setGreeting = (greeting) => ({
    type: SET_GREETING,
    greeting
})
