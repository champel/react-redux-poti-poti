import * as React from 'react'
import * as d3 from 'd3'
import { connect } from 'react-redux'
import { SFC } from 'react'
import { ResponsiveContainer, BarChart, Bar, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts'

import { getTopFirstNames } from './selectors'


export interface FirstnameCountsProps {
    data: object
}

export const FirstnameCounts: SFC<FirstnameCountsProps> = ({data}) =>
    <ResponsiveContainer width="90%" height={300}>
        <BarChart data={[data]} margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <YAxis/>
            <CartesianGrid strokeDasharray="3 3"/>
            <Tooltip />
            <Legend />
            {Object.keys(data).map((key, i) => <Bar key={key} dataKey={key} fill={d3.schemeCategory10[i]}/>)}
        </BarChart>
    </ResponsiveContainer>

export const enhance = (Component) => {
    const mapStateToProps = (state) => ({
        data: getTopFirstNames(state),
    })
    
    return connect(mapStateToProps)(Component)
}
  
export default enhance(FirstnameCounts)