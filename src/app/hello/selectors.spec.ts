import { expect } from 'chai'

import { getHellos, getValues, getPercentagesTable, getTopFirstNames, getGreeting, getFirstname, getLastname } from './selectors'

describe('hello', () => {
  const firstname = 'firstname'
  const lastname = 'lastname'

  describe('4. selectors', () => {
    it('should get an array of { "firstname", "lastname", "count" } of all hellos received', () => {
      const state = {
            hello: {
                allHellos: {
                    ['a']: {
                        count: 1,
                        firstname,
                        lastname
                    }
                }
            }
      }
      expect(getHellos(state))
        .to.deep.equal([{firstname, lastname, count:1}])
    })

    it('should get an array of values of the first name statistics', () => {
      expect(getValues({
        hello: {
          firstnameStatistics: {
            a: 1,
            b: 2
          }
        }
      })).to.deep.equal([1,2])
    })

    it('should obtain an array of ["firstname", percentage over the total]', () => {
      expect(getPercentagesTable({
        hello: {
          firstnameStatistics: {
            a: 1,
            b: 3
          }
        }
      })).to.deep.equal([
        ['a', 25],
        ['b', 75]
      ])
    })
  
    it('should obtain an object with the 3 top firstnames { ["firstname"] : counter }', () => {
      expect(getTopFirstNames({
        hello: {
          firstnameStatistics: {
            a: 1,
            b: 2,
            c: 3,
            d: 4
          }
        }
      })).to.deep.equal({
        d: 4,
        c: 3,
        b: 2
      })
    })

    it('should get the last setted "greeting"', () => {
      const greeting = "greeting"
      expect(getGreeting({
        hello: {
          greeting
        }
      })).to.deep.equal(greeting)
    })

    it('should get "firstname" of last hello submmited', () => {
      expect(getFirstname({
        hello: {
          lastHello: {
            firstname
          }
        }
      })).to.deep.equal(firstname)
    })
    
    it('should get "lastname" of last hello submmited', () => {
      const lastname = "lastname"
      expect(getLastname({
        hello: {
          lastHello: {
            lastname
          }
        }
      })).to.deep.equal(lastname)
    })
  })
})