import * as React from 'react'
import { mount, ReactWrapper } from 'enzyme'
import { stub } from 'sinon'
import { expect } from 'chai'
import * as renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store';

import * as selectors from './selectors'

import { enhance, FirstnameStatistics, FirstnameStatisticsProps } from './FirstnameStatistics';
import { createSink } from 'recompose';
import { Provider } from 'react-redux';
import { ResponsiveHalfDonut } from 'd3c';

const mockStore = configureStore([]);

describe('hello', () => {
  describe('5. components & containers', () => {
    const values = [1,2]
    const percentagesTable = [
      ['a', 25],
      ['b', 75]
    ]

    describe('FirstnameCounts component', () => {
      it('should show a barchart with a bar for each provided firstname', () => {
        const wrapper = mount(<FirstnameStatistics values={values} percentagesTable={percentagesTable}/>);
        expect(wrapper.find(ResponsiveHalfDonut).props().data).deep.equal(values)
      })
    })

    describe('FirstnameCounts enhancer', () => {

      it('should provide top first names', () => {
        const getValuesStub = stub(selectors, 'getValues').returns(values)
        const getPercentagesTableStub = stub(selectors, 'getPercentagesTable').returns(percentagesTable)

        const Sink = createSink((props: FirstnameStatisticsProps) => {
          expect(props).to.deep.include({
            values,
            percentagesTable
          })
        })

        const EnhancedSink = enhance(Sink)

        const store = mockStore()
        renderer.create(
          <Provider store={store}>
            <EnhancedSink/>
          </Provider>
        )
        getValuesStub.restore()
        getPercentagesTableStub.restore()
      })
    })
  })
})