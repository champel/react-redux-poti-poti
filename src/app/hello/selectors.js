import key from './key'
import { createStructuredSelector } from 'reselect'
import {
    createSelectorWithDependencies as createSelector
} from 'reselect-tools'

const rootSelector = (state) => state[key]

const allHellosSelector = createSelector(rootSelector, ({allHellos}) => allHellos)
export const getHellos = createSelector(allHellosSelector, (allHellos) => Object.values(allHellos))

const greetingSelector = createSelector(rootSelector, ({greeting}) => greeting)
export const getGreeting = createSelector(greetingSelector, (greeting) => greeting)

const lastHelloSelector = createSelector(rootSelector, ({lastHello}) => lastHello )
export const getFirstname = createSelector(lastHelloSelector, ({firstname}) => firstname)
export const getLastname = createSelector(lastHelloSelector, ({lastname}) => lastname)

export const getFirstnameAndLastname = createStructuredSelector({
    firstname: getFirstname,
    lastname: getLastname,
  })

export const percentagesTable = (firstnameStatistics) => {
    const total = Object.values(firstnameStatistics).reduce((accumulator, currentValue) => accumulator + currentValue, 0)
    return Object.entries(firstnameStatistics).map(([key, value]) => [key, Math.round(value*100/total, 0)])
}

export const topFirstnames = (firstnameStatistics) => Object.entries(firstnameStatistics)
    .map(([key, value]) => ({ firstname: key, count: value }))
    .sort((a,b) => (b.count - a.count))
    .slice(0,3)
    .reduce((previous, current) => ({
        ...previous,
        [current.firstname]: current.count
    }), {}
)

const firstnameStatisticsSelector = createSelector(rootSelector, ({firstnameStatistics}) => firstnameStatistics)
export const getPercentagesTable = createSelector(firstnameStatisticsSelector, percentagesTable)
export const getTopFirstNames = createSelector(firstnameStatisticsSelector, topFirstnames)
export const getValues = createSelector(firstnameStatisticsSelector, (firstnameStatistics) => Object.values(firstnameStatistics))

