import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import { key as helloKey, reducer as hello } from './hello'
import { key as devicesKey, reducer as devices } from './devices'
import { key as tagsKey, reducer as tags } from './tags'
import { key as languageKey, reducer as language } from './language'
import { key as layoutKey, reducer as layout } from './layout'

import { key as sessionStatisticsKey, reducer as sessionStatistics } from './session-statistics'

const reducers = combineReducers({
  routing: routerReducer,
  [devicesKey]: devices,
  [tagsKey]: tags,
  [languageKey]: language,
  [helloKey]: hello,
  [layoutKey]: layout,
  [sessionStatisticsKey]: sessionStatistics,
})

export default reducers

