import * as React from 'react'
import { SFC } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { translate } from 'react-i18next';
import { compose } from 'recompose'

import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

import RouterIcon from '@material-ui/icons/Router';
import PollIcon from '@material-ui/icons/Poll';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import SendIcon from '@material-ui/icons/Send';
import PeopleIcon from '@material-ui/icons/People';

const DefaultMenu: SFC<{dispatch: Function, t: Function}> = ({dispatch, t}) =>
  <>
      <ListItem button onClick={() => dispatch(push('/devices'))}>
        <ListItemIcon>
          <RouterIcon />
        </ListItemIcon>
        <ListItemText primary={t('devices')} />
      </ListItem>
      <ListItem button onClick={() => dispatch(push('/deviceStatistics'))}>
        <ListItemIcon>
          <PollIcon />
        </ListItemIcon>
        <ListItemText primary={t('deviceStatistics')} />
      </ListItem>
      <ListItem button onClick={() => dispatch(push('/sessionStatistics'))}>
        <ListItemIcon>
          <ShowChartIcon />
        </ListItemIcon>
        <ListItemText primary={t('sessionStatistics')} />
      </ListItem>
      <ListItem button onClick={() => dispatch(push('/'))}>
        <ListItemIcon>
          <SendIcon />
        </ListItemIcon>
        <ListItemText primary={t('campaigns')} />
      </ListItem>
      <ListItem button onClick={() => dispatch(push('/about'))}>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary={t('about')} />
      </ListItem>
  </>

const enhance = compose(
    translate('app'),
    connect()
)

export default enhance(DefaultMenu);