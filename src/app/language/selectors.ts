import key from './key'
import { createSelector } from "reselect"

const root = (state) => state[key]

export const isLanguageMenuOpen = createSelector(root, ({menu}) => menu)
