import { OPEN_LANGUAGE_MENU, CLOSE_LANGUAGE_MENU } from '../constants'

const menu = (state = false, action) => {
    switch (action.type) {
      case OPEN_LANGUAGE_MENU:
        return true;
      case CLOSE_LANGUAGE_MENU:
        return false;
      default:
        return state;
    }
  }

export default menu

