import { OPEN_LANGUAGE_MENU, CLOSE_LANGUAGE_MENU } from './constants'

export const openLanguageMenu = () => ({
    type: OPEN_LANGUAGE_MENU
})
  
export const closeLanguageMenu = () => ({
    type: CLOSE_LANGUAGE_MENU
})
  
  