import key from './key'
export const OPEN_LANGUAGE_MENU = `@${key}/menu/OPEN`
export const CLOSE_LANGUAGE_MENU = `@${key}/menu/CLOSE`
