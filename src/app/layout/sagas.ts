import { takeEvery } from 'redux-saga/effects'

import { TOGGLE_MENU } from './constants'

// Hack, dispatch a resize event after a TOGGLE_MENU, needed for d3/responsiveWrapper
function* resizer() {
  yield takeEvery(TOGGLE_MENU, action => window.setTimeout(() => window.dispatchEvent(new Event('resize')), 300));
}

export default [ resizer ]