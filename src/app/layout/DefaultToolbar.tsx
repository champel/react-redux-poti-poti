import * as React from 'react'
import * as classNames from 'classnames';

import { connect } from 'react-redux'
import { translate } from 'react-i18next';
import { compose } from 'recompose'
import { SFC, ComponentClass } from 'react';

import { withStyles, createStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'

import MenuIcon from '@material-ui/icons/Menu'
import AccountCircle from '@material-ui/icons/AccountCircle'

import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import { isMenuOpen } from './selectors'
import { toggleMenu } from './actions'

import { isLanguageMenuOpen } from '../language/selectors'
import { openLanguageMenu, closeLanguageMenu } from '../language/actions'

import { drawerWidth } from './DefaultDrawer'
import { createStructuredSelector } from 'reselect';

const styles = (theme: Theme) => createStyles({
    appBar: {
        position: 'absolute',
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
});
  
const DefaultToolbar: SFC<any> =  ({title, isMenuOpen, onToggleMenu, isLanguageMenuOpen, onOpenLanguageMenu, onCloseLanguageMenu, onChangeLanguage, onSearch, classes, _}) =>
    <AppBar className={classNames(classes.appBar, isMenuOpen && classes.appBarShift)}>
        <Toolbar disableGutters={!isMenuOpen}>
            <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={() => onToggleMenu()}
                className={classNames(classes.menuButton, isMenuOpen && classes.hide)}
            >
                <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" noWrap>
                { title || "Poti poti" }
            </Typography>
            <IconButton
                  aria-owns={open ? 'menu-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={false}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                  <MenuItem onClick={this.handleClose}>My account</MenuItem>
            </Menu>
        </Toolbar>
    </AppBar>

const mapStateToProps = createStructuredSelector({
    isMenuOpen,
    isLanguageMenuOpen
})
  
const mapDispatchToProps = (dispatch, props) => ({
    _: props.t,
    onToggleMenu: () => dispatch(toggleMenu()),
    onOpenLanguageMenu: () => dispatch(openLanguageMenu()),
    onCloseLanguageMenu: () => dispatch(closeLanguageMenu()),
    onChangeLanguage: (language) => props.i18n.changeLanguage(language)
})
  
const enhance = compose(
    translate('app'),
    connect(mapStateToProps, mapDispatchToProps),
    withStyles(styles, { withTheme: true }),
)

export interface DefaultToolbarProps {
    title?: string,
    onSearch?: Function,
}

export default enhance(DefaultToolbar) as ComponentClass<DefaultToolbarProps>;