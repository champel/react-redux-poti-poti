import * as React from 'react'

import * as classNames from 'classnames';
import { compose } from 'recompose'
import { connect } from 'react-redux'

import { withStyles, createStyles, Theme } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'

import { isMenuOpen } from './selectors'
import { toggleMenu } from './actions'

export const drawerWidth = 240;

const styles = (theme: Theme) => createStyles({
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    width: 60,
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerInner: {
    // Make the items inside not wrap when transitioning:
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
})

const DefaultDrawer: React.SFC<any> = ({header, menu, isMenuOpen, onToggleMenu, classes, theme}) =>
    <Drawer
      variant="permanent"
      open={isMenuOpen}
      classes={{paper: classNames(classes.drawerPaper, !isMenuOpen && classes.drawerPaperClose)}}
    >
        <div className={classes.drawerInner}>
            <div className={classes.drawerHeader}>
                <IconButton onClick={() => onToggleMenu()}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                </IconButton>
            </div>
            <Divider />
            {menu}
        </div>
    </Drawer>

const mapStateToProps = (state) => ({
    isMenuOpen: isMenuOpen(state),
})

const mapDispatchToProps = (dispatch, props) => ({
  onToggleMenu: () => dispatch(toggleMenu()),
})
  
const enhance = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withStyles(styles, { withTheme: true })
)

export interface DefaultDrawerProps {
  header?: React.ReactNode
  menu?: React.ReactNode
}

export default enhance(DefaultDrawer) as React.ComponentClass<DefaultDrawerProps>