import * as React from 'react'
import { compose } from 'recompose'
import { withStyles, createStyles, Theme } from '@material-ui/core/styles'

import DefaultToolbar from './DefaultToolbar'
import DefaultDrawer from './DefaultDrawer'

const styles = (theme: Theme) => createStyles({
  root: {
    width: '100%',
    height: '100%',
    minHeight: '100%',
    zIndex: 1,
    overflow: 'hidden',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: 24,
    height: 'calc(100% - 56px)',
    marginTop: 56,
    paddingBottom: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      marginTop: 64,
    },
    overflow: 'scroll',
  },
});

const DefaultLayout: React.SFC<any> = ({title, menu, toolbar = <DefaultToolbar title={title}/>, children, classes}) =>
    <div className={classes.root}>
        <div className={classes.appFrame}>
            {toolbar}
            <DefaultDrawer menu={menu} />
            <main className={classes.content}>
              {children}
              <br/>
              <br/>
            </main>
        </div>
    </div>

const enhance = compose(
    withStyles(styles, { withTheme: true })
)

export interface DefaultLayoutProps {
  title?: string | React.ReactNode
  left?: React.ReactNode
  toolbar?: React.ReactNode
  menu?: React.ReactNode
}

export default enhance(DefaultLayout) as React.ComponentClass<DefaultLayoutProps>