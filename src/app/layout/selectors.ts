import { createSelector } from 'reselect'

import key from './key'

const layoutSelector = (state) => state[key]
export const isMenuOpen = createSelector(layoutSelector, ({menuOpen}) => menuOpen)
