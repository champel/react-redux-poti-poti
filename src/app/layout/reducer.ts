import { combineReducers } from 'redux';

import { TOGGLE_MENU } from './constants'

const menuOpen = (state = false, action): boolean => {
    switch (action.type) {
      case TOGGLE_MENU:
        return !state;
      default:
        return state;
    }
  }
  
export default combineReducers({menuOpen})