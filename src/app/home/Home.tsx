import * as React from 'react'

import { Grid, Row, Col } from 'react-flexbox-grid'
import { Card,CardHeader,  CardContent, Typography } from '@material-ui/core';

import { Greeting, HelloForm, FirstnameStatistics, FirstnameCounts } from '../hello'

const StandardColumn = ({children}) =>
  <Col xs={12} sm={6} md={6} lg={4}>
    {children}
  </Col>

const MyCard = ({children, ...params}) => <Card style={{marginBottom: 10}} {...params}>{children}</Card>

const AnotherColumn = ({children}) =>
  <StandardColumn>
    <MyCard>
      <CardHeader title="Another column" subheader="to test the responsiveness"/>
      <CardContent>
        {children}
      </CardContent>
    </MyCard>
  </StandardColumn>

const Home = () =>
    <Grid fluid>
      <Row>
        <StandardColumn>
          <MyCard data-testid="cardGreeting">
            <CardHeader title="Who are you?" subheader="enter your name and last name"/>
            <CardContent>
              <HelloForm />
              <br/>
              <Greeting />
            </CardContent>
          </MyCard>
        </StandardColumn>
        <StandardColumn>
          <MyCard>
            <CardHeader title="D3 Responsive Visualization" subheader="percentage by firstname"/>
            <CardContent>
              <FirstnameStatistics/>
            </CardContent>
          </MyCard>
        </StandardColumn>
        <StandardColumn>
          <MyCard>
            <CardHeader title="Recharts Visualization" subheader="top 3 firstnames"/>
            <CardContent>
              <FirstnameCounts/>
            </CardContent>
          </MyCard>
        </StandardColumn>
        <AnotherColumn>
          <img src="https://picsum.photos/400/100?random" width="100%"/>
        </AnotherColumn>
        <AnotherColumn>
          <img src="https://picsum.photos/400/200?random" width="100%"/>
        </AnotherColumn>
        <AnotherColumn>
          <HelloForm />
        </AnotherColumn>
      </Row>
    </Grid>

export default Home