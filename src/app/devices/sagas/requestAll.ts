import { delay } from 'redux-saga'
import { put, takeEvery } from 'redux-saga/effects'

import { REQUEST_ALL } from '../constants'
import { fetchAll } from '../actions'
import { payload } from '@/fixtures/devices'

function* delayedFetch() {
  yield delay(500)
  yield put(fetchAll(payload))
}

function* loader() {
  yield takeEvery(REQUEST_ALL, delayedFetch);
}

export default loader