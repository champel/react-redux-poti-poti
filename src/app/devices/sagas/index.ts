import requestAll from './requestAll'
import requestDetails from './requestDetails'

export default [ requestAll, requestDetails ]