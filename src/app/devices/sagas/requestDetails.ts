import { delay } from 'redux-saga'
import { put, takeEvery } from 'redux-saga/effects'

import { REQUEST_DETAILS } from '../constants'
import { fetchDetails } from '../actions'
import { byId } from '@/fixtures/devices'

function* delayedFetch({id}: any) {
  yield delay(1000)
  yield put(fetchDetails(id, byId[id]))
}

function* loader() {
  yield takeEvery(REQUEST_DETAILS, delayedFetch);
}

export default loader