import * as React from 'react'
import { SFC } from 'react';
import { compose } from 'recompose'
import { connect } from 'react-redux'

import { Table, TableBody, TableCell, TableHead, TableRow, LinearProgress, Typography } from '@material-ui/core'
import { createStructuredSelector } from 'reselect'

import withDevicesLoaded from './withDevicesLoaded'

import { isLoading, isLoaded, getAll } from './selectors'

import { TagChips, withTagsLoaded } from '../tags'

export interface DevicesListContainerProps {
    onSelectDevice: Function
}

export interface DevicesListProps extends DevicesListContainerProps {
    isLoaded: boolean,
    isLoading: boolean,
    devices: any,
}

export const DevicesList: SFC<DevicesListProps> = ({ isLoaded, isLoading, devices, onSelectDevice }) => (
    <div>
        { isLoading && <LinearProgress/> }
        { isLoaded && 
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Serial Number</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Tags</TableCell>
                        <TableCell>Status</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {devices.map(device =>
                        <TableRow key={device.uuid} hover onClick={() => onSelectDevice(device)}>
                            <TableCell>{device.uuid}</TableCell>
                            <TableCell>{device.name}</TableCell>
                            <TableCell>
                                <TagChips ids={device.tags} />
                            </TableCell>
                            <TableCell>{device.status}</TableCell>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
                    
        }
        { !isLoaded && <Typography variant="display1">Loading devices...</Typography> }
    </div>
);

const mapStateToProps = createStructuredSelector({
    isLoading: isLoading,
    isLoaded: isLoaded,
    devices: getAll
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onSelectDevice: (device) => ownProps.onSelectDevice(device)
})

const enhance = compose(
    withTagsLoaded,
    withDevicesLoaded,
    connect(mapStateToProps, mapDispatchToProps),
)

export default enhance(DevicesList) as React.ComponentClass<DevicesListContainerProps>