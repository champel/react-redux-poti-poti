import key from './key'
import { createSelector } from 'reselect'

const devicesSelector = (state: any) => state[key]

const getById = createSelector(devicesSelector, ({byId}) => byId)
const getAllIds = createSelector(devicesSelector, ({allIds}) => allIds)

const isLoading = createSelector(devicesSelector, ({loading}) => loading)
const isLoaded = createSelector(devicesSelector, ({loaded}) => loaded)

const getAll = createSelector(getAllIds, getById, (allIds, byId) =>
  allIds.map(deviceId => byId[deviceId]))

const getOne = (deviceIdSelector) => createSelector(getById, deviceIdSelector, (devicesById, deviceId: string) => devicesById[deviceId])

export { isLoading, isLoaded, getAll, getOne }