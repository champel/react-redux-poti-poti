import { loadIfNotLoadingOrLoaded } from '@/loaders'

import { isLoaded, isLoading } from './selectors'
import { requestAll } from './actions';

export default loadIfNotLoadingOrLoaded(isLoading, isLoaded, requestAll)