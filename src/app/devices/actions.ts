import { FETCH_ALL, FETCH_DETAILS, REQUEST_ALL, REQUEST_DETAILS } from './constants'

export const requestAll = () => ({
    type: REQUEST_ALL
})

export const fetchAll = (payload) => ({
    type: FETCH_ALL,
    payload
})

export const requestDetails = (id) => ({
    type: REQUEST_DETAILS,
    id
})

export const fetchDetails = (id, payload) => ({
    type: FETCH_DETAILS,
    id,
    payload
})
