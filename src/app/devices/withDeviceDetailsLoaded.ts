import { requestDetails } from "./actions";
import { getOne } from "./selectors";
import { loadIfNotExists } from '@/loaders'

export default (getId) => loadIfNotExists(getId, getOne, requestDetails)
