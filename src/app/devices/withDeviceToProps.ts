import { createStructuredSelector } from 'reselect'
import { getOne } from './selectors'
import { connect } from 'react-redux'

const mapStateToProps = (deviceIdSelector) => createStructuredSelector({
    device: getOne(deviceIdSelector)
})

const mapDispatchToProps = (dispatch, ownProps) => ({
})

export default (deviceIdSelector) => connect(mapStateToProps(deviceIdSelector), mapDispatchToProps)