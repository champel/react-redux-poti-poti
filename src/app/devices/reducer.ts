import { combineReducers } from 'redux'

import { FETCH_ALL, FETCH_DETAILS, REQUEST_ALL } from './constants'

const loading = (state = false, action) => {
    switch (action.type) {
        case REQUEST_ALL:
            return true
        case FETCH_ALL:
            return false
        default:
            return state
    }
}

const loaded = (state = false, action) => {
    switch (action.type) {
        case FETCH_ALL:
            return true
        default:
            return state
    }
}

const byId = (state = {}, action) => {
  switch (action.type) {
    case FETCH_ALL:
      return action.payload.entities.devices
    case FETCH_DETAILS:
      return {
            ...state,
            [action.id]: {
                ...state[action.id],
                ...action.payload
            }
        }
  default:
      return state
  }
}

const allIds = (state = [], action) => {
    switch (action.type) {
      case FETCH_ALL:
        return action.payload.result
    default:
        return state
    }
  }
  
const reducer = combineReducers({
    loading,
    loaded,
    byId,
    allIds
})

export default reducer