import { createStructuredSelector } from 'reselect'
import { getAllIds } from './selectors'
import { connect } from 'react-redux'

const mapStateToProps = (prop) => createStructuredSelector({
    [prop]: getAllIds
})

const mapDispatchToProps = (dispatch, ownProps) => ({
})

export default (prop) => connect(mapStateToProps(prop), mapDispatchToProps)