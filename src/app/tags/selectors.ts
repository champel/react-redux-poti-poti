import { createSelector } from "reselect";

import key from './key'

const tagsSelector = (state) => state[key]

export const getById = createSelector(tagsSelector, ({byId}) => byId)
export const getAllIds = createSelector(tagsSelector, ({allIds}) => allIds)

export const getAll = createSelector(getAllIds, getById, (allIds, byId) => allIds.map(id => byId[id]))

export const isLoaded = createSelector(tagsSelector, ({loaded}) => loaded)
export const isLoading = createSelector(tagsSelector, ({loading}) => loading)