import { REQUEST_ALL, FETCH_ALL } from './constants'

export const requestAll = () => ({
    type: REQUEST_ALL
})

export const fetchAll = (payload) => ({
    type: FETCH_ALL,
    payload
})
