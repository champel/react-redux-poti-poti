import { delay } from 'redux-saga'
import { put, takeLatest } from 'redux-saga/effects'

import { REQUEST_ALL } from './constants'
import { fetchAll } from './actions'

import { payload } from '@/fixtures/tags'

function* delayedFetch() {
  yield delay(2000)
  yield put(fetchAll(payload))
}

function* loader() {
  yield takeLatest(REQUEST_ALL, delayedFetch);
}

export default [ loader ]