import * as React from 'react'
import { SFC } from "react"
import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect';
import { compose } from 'recompose';

import { Chip } from '@material-ui/core'

import { getById } from './selectors';

const TagChip: SFC<{id: string, tag: any}> = ({ id, tag }) => (
    <Chip label={tag ? tag.name : 'loading...'} />
)

const enhance = compose(
    connect(createStructuredSelector({
        tag: createSelector(
            getById,
            (_, props) => props.id,
            (byId, id) => byId[id])
    })),
)

export default enhance(TagChip) as React.ComponentClass<{id: string}>