import { expect } from 'chai'

import { getById, getAllIds } from './selectors'

import * as fixtures from './fixtures'

describe('tags', () => {
    describe('selectors', () => {
        it('should get the indexed elements', () => {
            expect(getById({tags: fixtures.filledTagsState})).eql(fixtures.tagsById)
        })
        it('should get all the identifiers', () => {
            expect(getAllIds({tags: fixtures.filledTagsState})).eql(fixtures.allTagIds)
        })
    })
})