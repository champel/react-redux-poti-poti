import { FETCH_ALL } from './constants'

export const tag1 = {
    id: 'tag1',
    name: 'Tag 1',
    color: 'red'
}
export const tag2 = {
    id: 'tag2',
    name: 'Tag 2',
    color: 'red'
}
export const tag3 = {
    id: 'tag3',
    name: 'Tag 3',
    color: 'blue'
}

export const tagsById = {
    [tag1.id]: tag1,
    [tag2.id]: tag2,
    [tag3.id]: tag3,
}
export const allTagIds = [tag1.id, tag2.id, tag3.id]
export const filledTagsState = { loading: false, loaded: true, byId: tagsById, allIds: allTagIds }
export const emptyTagsState = {loading: false, loaded: false, allIds: [], byId: {}}

export const tagsPayload = {
    entities: {
    tags: tagsById
    },
    result: allTagIds
}

export const fetchAllTagsAction = {
    type: FETCH_ALL,
    payload: tagsPayload
}

export const allTags = [ tag1, tag2, tag3 ]
