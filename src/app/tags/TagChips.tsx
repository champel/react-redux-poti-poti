import * as React from 'react'
import { SFC } from "react";

import TagChip from './TagChip';

const TagChips: SFC<{ids: any}> = ({ ids }) => (
    <div>
        {ids.map(id => <TagChip key={"tag:"+id} id={id} />)}
    </div>
)

export default TagChips