import { expect } from 'chai'

import { fetchAll } from './actions'

import * as fixtures from './fixtures'

describe('tags', () => {
    describe('actions', () => {
        it('should fetch all', () => {
            expect(fetchAll(fixtures.tagsPayload)).eql(fixtures.fetchAllTagsAction)
        })
    })
})