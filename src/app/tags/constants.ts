import key from './key'

const ENTITY = `@${key}/`
export const REQUEST_ALL = ENTITY + 'REQUEST_ALL'
export const FETCH_ALL = ENTITY + 'FETCH_ALL'

