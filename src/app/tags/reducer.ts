
import { combineReducers } from 'redux'

import { REQUEST_ALL, FETCH_ALL } from './constants'

const loading = (state = false, action): boolean => {
    switch (action.type) {
        case REQUEST_ALL:
            return true
        case FETCH_ALL:
            return false
        default:
            return state
    }
}

const loaded = (state = false, action): boolean => {
    switch (action.type) {
        case FETCH_ALL:
            return true
        default:
            return state
    }
}

const byId = (state = {}, action): object => {
  switch (action.type) {
    case FETCH_ALL:
      return action.payload.entities.tags
  default:
      return state
  }
}

const allIds = (state = [], action): string[] => {
    switch (action.type) {
      case FETCH_ALL:
        return action.payload.result
    default:
        return state
    }
  }
  
export default combineReducers({
    loading,
    loaded,
    byId,
    allIds
})

