import { expect } from 'chai'

import reducer from './reducer'

import * as fixtures from './fixtures'

describe('tags', () => {
    describe('reducer', () => {
        it('should return an empty object as initial state', () => {
            expect(reducer(undefined, {} as any)).eql(fixtures.emptyTagsState)
        })

        it('should handle FETCH_ALL filling the state', () => {
            expect(reducer(fixtures.emptyTagsState, fixtures.fetchAllTagsAction)).to.deep.equal(fixtures.filledTagsState)
        })
    })
})