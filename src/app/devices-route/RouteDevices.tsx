import * as React from 'react'
import { Route } from 'react-router'
import { withRouter } from 'react-router'

import { Typography } from '@material-ui/core';

import { DevicesList } from '../devices'
import { DeviceDetails, DeviceName } from '../device'

import DefaultLayout from '../layout/DefaultLayout'

export const DevicesPage = (root, menu, history) => () => (
    <DefaultLayout menu={menu}>
        <Typography variant="headline" gutterBottom>Devices</Typography>
        <DevicesList onSelectDevice={(device) => history.push(`${root}/${device.uuid}`)}/>
    </DefaultLayout>
);

export const DevicePage = (menu) => ({match}) => (
    <DefaultLayout title={<span>Device <DeviceName uuid={match.params.uuid}/></span>} menu={menu}>
        <Typography variant="headline" gutterBottom>Device details</Typography>
        <DeviceDetails uuid={match.params.uuid}/>
    </DefaultLayout>
);

export const RouteDevices = ({root, menu, history}) => (
    <>
        <Route exact path={`${root}/:uuid`} component={DevicePage(menu)}/>
        <Route exact path={root} component={DevicesPage(root, menu, history)}/>
    </>
)

export default withRouter(RouteDevices) as React.ComponentClass<{root: string, menu: React.ReactElement<any>}>