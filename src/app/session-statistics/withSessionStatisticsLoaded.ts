import { loadIfNotLoadingOrLoaded } from "loaders";

import { request } from './actions'
import { loaded, loading } from './selectors'

export default loadIfNotLoadingOrLoaded(loading, loaded, request)