import { createSelector, createStructuredSelector } from "reselect";

import key from './key'
import { SessionStatistics, SessionStatistic, StatisticValuesByType } from './reducer'

const rootSelector = (state) => state[key]

export const getSelectedGender = createSelector(rootSelector, ({genderSelection}) => genderSelection)
export const getSelectedCard = createSelector(rootSelector, ({cardSelection}) => cardSelection)
export const getSelectedAgeRange = createSelector(rootSelector, ({ageRangeSelection}) => ageRangeSelection)
export const getSelectedTag = createSelector(rootSelector, ({tagSelection}) => tagSelection)

const statisticsReducer = (memo: StatisticValuesByType, { statistics }: SessionStatistic): StatisticValuesByType => {
    Object.keys(statistics).forEach(statisticKey => {
        if (!memo[statisticKey]) {
            memo[statisticKey] = {}
        }
        Object.keys(statistics[statisticKey]).forEach(statisticValueKey => {
            if (!memo[statisticKey][statisticValueKey]) {
                memo[statisticKey][statisticValueKey] = 0
            }
            memo[statisticKey][statisticValueKey] += statistics[statisticKey][statisticValueKey]
        })
    })
    return memo
}

export interface Filters {
    gender: string
    card: boolean
    ageRange: number[]
}

export const getByFilters = ({allIds, byId}: SessionStatistics, { gender, card, ageRange }: Filters): StatisticValuesByType => {
    return allIds.map(id => byId[id])
        .filter(sessionStatistics => 
            (card == null || (sessionStatistics.card == card)) &&
            (gender == null || (sessionStatistics.gender == gender)) &&
            (ageRange == null || (
                (!ageRange[0] || sessionStatistics.age >= ageRange[0]) &&
                (!ageRange[1] || sessionStatistics.age <= ageRange[1]))))
        .reduce(statisticsReducer, {})
}

const sessionStatisticsSelector = createSelector(rootSelector, ({sessionStatistics}) => sessionStatistics)

const filtersSelector = createStructuredSelector({
    gender: getSelectedGender,
    card: getSelectedCard,
    ageRange: getSelectedAgeRange
})

export const byFilters = createSelector(sessionStatisticsSelector, filtersSelector, getByFilters)
export const loaded = createSelector(sessionStatisticsSelector, ({loaded}: SessionStatistics) => loaded)
export const loading =  createSelector(sessionStatisticsSelector, ({loading}: SessionStatistics) => loading)

