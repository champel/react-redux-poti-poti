import * as React from 'react'
import { SFC } from 'react';
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Card, CardHeader, CardContent, LinearProgress, Typography } from '@material-ui/core';
import { ResponsiveHalfDonut } from '@/d3c'

import { loaded, loading, byFilters } from './selectors'

import { getPercentagesTable} from './percentagesTable'
import withSessionStatisticsLoaded from './withSessionStatisticsLoaded';
import { createStructuredSelector } from 'reselect';

interface SessionStatisticsPanelProps {
    isLoaded: boolean,
    isLoading: boolean,
    statistics: any
}

const colors = ["#0bd318", "#ffdb4c", "#ff8800", "#ff323e", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"]

const values = (statistic) => Object.keys(statistic).map(id => statistic[id])

const PercentagesTable = ({data, width, maxWidth, colors}) =>
    <table style={{width: width, margin:'auto', maxWidth: maxWidth}}>
        <tbody>
            {data.map(([key, value], i) => <tr key={key}>
                <td>
                    <Typography>
                        <svg width="20" height="12">
                            <rect width="12" height="12" style={{fill:colors[i]}} />
                        </svg>
                        {key ? key : "<no name>"}
                    </Typography>
                </td>
                <td style={{textAlign:"right"}}><Typography>{value}%</Typography></td>
            </tr>)}
        </tbody>
    </table>


const SessionStatisticsPanel: SFC<SessionStatisticsPanelProps> = ({ isLoaded, isLoading, statistics }) => (
    <div>
        <Grid fluid>
            { isLoading && !isLoaded && <Row><Col xs={12}><LinearProgress variant="indeterminate"></LinearProgress></Col></Row> }
            { isLoaded &&
                <Row>
                    { Object.keys(statistics).length > 0 && Object.keys(statistics).map(statistic =>
                            <Col key={statistic} xs={12} sm={6} md={6} lg={4}>
                                <Card style={{marginBottom: 10}}>
                                    <CardHeader title={statistic}/>
                                    <CardContent>
                                        <ResponsiveHalfDonut data={values(statistics[statistic])} height={200} width={200} colors={colors}/>
                                        <PercentagesTable data={getPercentagesTable(statistics[statistic])} width="90%" maxWidth={380} colors={colors} />
                                    </CardContent>
                                </Card>
                                {isLoading && <LinearProgress/>}
                            </Col>)
                    }
                    { !Object.keys(statistics).length && <Col xs={12}>No data matching the specified criteria</Col>}
                </Row>
            }
            { !isLoaded &&
                <Row>
                    <Col xs={12}><Typography variant="display2">Loading statistics...</Typography></Col>
                </Row>
            }
        </Grid>
    </div>
);

const enhance = compose(
    withSessionStatisticsLoaded,
    connect(createStructuredSelector({
        isLoading: loading,
        isLoaded: loaded,
        statistics: byFilters
    }))
)

export default enhance(SessionStatisticsPanel)