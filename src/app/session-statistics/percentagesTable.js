export const getPercentagesTable = (measurement) => {
    const total = Object.values(measurement).reduce((accumulator, currentValue) => accumulator + currentValue, 0)
    return Object.entries(measurement).map(([key, value]) => [key, Math.round(value*100/total, 0)])
}
