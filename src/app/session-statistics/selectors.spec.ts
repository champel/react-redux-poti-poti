import { expect } from 'chai'

import { getByFilters, getSelectedCard, getSelectedAgeRange, getSelectedGender, getSelectedTag } from './selectors'

import * as fixtures from './reducer/sessionStatistics.fixtures'

describe('session-statistics', () => {
    describe('selectors', () => {
        it('should get the indexed elements', () => {
            expect(getByFilters(fixtures.filledStatisticsState, {} as any)).eql(fixtures.statisticsWithoutFilters)
        })

        it('should get the filtered indexed elements', () => {
            expect(getByFilters(fixtures.filledStatisticsState, {card: true, gender:'women', ageRange: [10,20]})).eql(fixtures.statisticsByWomen)
        })

        it('should return the selected ageRange', () => {
            const ageRangeSelection = [10,20]
            expect(getSelectedAgeRange({
                sessionStatistics: {
                    ageRangeSelection
                }
            })).to.deep.equal(ageRangeSelection)
        })

        it('should return the selected card', () => {
            const cardSelection = true
            expect(getSelectedCard({
                sessionStatistics: {
                    cardSelection
                }
            })).to.deep.equal(cardSelection)
        })

        it('should return the selected gender', () => {
            const genderSelection = 'women'
            expect(getSelectedGender({
                sessionStatistics: {
                    genderSelection
                }
            })).to.deep.equal(genderSelection)
        })

        it('should return the selected tag', () => {
            const tagSelection = 'tag1'
            expect(getSelectedTag({
                sessionStatistics: {
                    tagSelection
                }
            })).to.deep.equal(tagSelection)
        })
    })
})