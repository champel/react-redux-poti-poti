import * as React from 'react'
import { SFC } from 'react';
import { compose } from 'recompose'
import { connect } from 'react-redux'

import { selectGender, selectCard } from './actions'
import { getSelectedGender, getSelectedCard, getSelectedAgeRange } from './selectors'

import { Grid, Row, Col } from 'react-flexbox-grid'
import { FormControl, Select, MenuItem, InputLabel} from '@material-ui/core'
import { createSelector } from 'reselect';

export interface SessionStatisticsSelectorsProps {
    gender: string
    card: string
    ageRange: [ number, number ]
    onSelectGender: Function
    onSelectCard: Function
}

const encodeGender = (gender) => gender == null ? 'all' : gender
const decodeGender = (gender) => gender == 'all' ? null : gender

const encodeCard = (card) => card == null ? 'all' : (card ? 'with' : 'without')
const decodeCard = (card) => {
    switch(card) {
        case 'all': return null;
        case 'with': return true;
        case 'without': return false;
    }
}

export const SessionStatisticsSelectors: SFC<SessionStatisticsSelectorsProps> = ({ gender, card, ageRange, onSelectGender, onSelectCard }) => (
    <Grid fluid>
        <Row>
            <Col xs={12}>
                <FormControl>
                    <InputLabel>Gender</InputLabel>
                    <Select
                        value={encodeGender(gender)}
                        onChange={evt => onSelectGender(decodeGender(evt.target.value))}
                        name="gender"
                    >
                        <MenuItem value="all"><em>All</em></MenuItem>
                        <MenuItem value="women">Women</MenuItem>
                        <MenuItem value="man">Man</MenuItem>
                    </Select>
                </FormControl>
                &nbsp;     
                <FormControl>
                    <InputLabel>Card</InputLabel>
                    <Select
                        value={encodeCard(card)}
                        onChange={evt => onSelectCard(decodeCard(evt.target.value))}
                        name="card"
                    >
                        <MenuItem value="all"><em>All</em></MenuItem>
                        <MenuItem value="with">With card</MenuItem>
                        <MenuItem value="without">Without card</MenuItem>
                    </Select>
                </FormControl>        
            </Col>
        </Row>
    </Grid>
);

const mapStateToProps = (state) => createSelector(
    getSelectedGender, getSelectedCard, getSelectedAgeRange, 
    (gender, card, ageRange) => ({gender, card, ageRange}))

const mapDispatchToProps = dispatch =>  ({
    onSelectGender: (gender) => dispatch(selectGender(gender)),
    onSelectCard: (card) => dispatch(selectCard(card))
})

const enhance = compose(
    connect(mapStateToProps, mapDispatchToProps),
)

export default enhance(SessionStatisticsSelectors)