import { expect } from 'chai'

import reducer from './sessionStatistics'
import { fetchAll } from '../actions'


import * as fixtures from './sessionStatistics.fixtures'

describe('session-statistics', () => {
    const fetchAllStatisticsAction = fetchAll(fixtures.statisticsPayload)

    describe('sessionStatistics reducer', () => {
        it('should return an empty object as initial state', () => {
            expect(reducer(undefined, {} as any)).eql(fixtures.emptyStatisticsState)
        })
        it('should handle FETCH_ALL filling the state', () => {
            expect(reducer(fixtures.emptyStatisticsState, fetchAllStatisticsAction)).to.deep.equal(fixtures.filledStatisticsState)
        })
    })
})