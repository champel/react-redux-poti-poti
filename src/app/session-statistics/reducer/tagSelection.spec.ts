import { expect } from 'chai'

import reducer from './tagSelection'
import { SELECT_TAG } from '../constants'

describe('session-statistics', () => {
    describe('tagSelection reducer', () => {
        it('should be initially be unselected', () => {
            expect(reducer(undefined, {})).eql(null)
        })

        it('should handle SELECT_TAG', () => {
            const tag = 'tag1'
            const selectTagAction = {
                type: SELECT_TAG,
                tag
            }
            expect(reducer(true, selectTagAction)).to.deep.equal(tag)
        })
    })
})
