import { SELECT_AGE_RANGE } from '../constants'

const reducer = (state = null, action) => {
    switch (action.type) {
      case SELECT_AGE_RANGE:
        return action.ageRange;
      default:
        return state;
    }
  }

export default reducer
