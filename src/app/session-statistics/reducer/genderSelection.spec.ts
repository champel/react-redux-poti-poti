import { expect } from 'chai'

import reducer from './genderSelection'
import { SELECT_GENDER } from '../constants'

describe('session-statistics', () => {
    describe('gender reducer', () => {
        it('should be initially be unselected', () => {
            expect(reducer(undefined, {})).eql(null)
        })

        it('should handle SELECT_GENDER', () => {
            const gender = 'women'
            const selectGenderAction = {
                type: SELECT_GENDER,
                gender
            }
            expect(reducer(true, selectGenderAction)).to.deep.equal(gender)
        })
    })
})
