import { combineReducers } from 'redux'

import { TypeKeys } from '../constants'
import { ActionTypes, SessionStatistic, StatisticValuesByType } from '../actions'

const loading = (state: boolean = false, action: ActionTypes): boolean => {
    switch (action.type) {
        case TypeKeys.REQUEST:
            return true
        case TypeKeys.FETCH_ALL:
            return false
        default:
            return state
    }
}

const loaded = (state: boolean = false, action: ActionTypes): boolean => {
    switch (action.type) {
        case TypeKeys.FETCH_ALL:
            return true
        default:
            return state
    }
}

const byId = (state: { [id:string]: SessionStatistic } = {}, action: ActionTypes): { [id:string]: SessionStatistic } => {
  switch (action.type) {
    case TypeKeys.FETCH_ALL:
      return action.payload.entities.sessionStatistics
  default:
      return state
  }
}

const allIds = (state: string[] = [], action: ActionTypes): string[] => {
    switch (action.type) {
      case TypeKeys.FETCH_ALL:
        return action.payload.result
    default:
        return state
    }
  }
  
interface State {
    loading: boolean,
    loaded: boolean,
    byId: { [id:string]: SessionStatistic },
    allIds: string[]
}

export { State, SessionStatistic, StatisticValuesByType }

export default combineReducers({
    loading,
    loaded,
    byId,
    allIds
})

