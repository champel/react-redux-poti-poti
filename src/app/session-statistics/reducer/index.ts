import { combineReducers } from 'redux'

import tagSelection from './tagSelection'
import cardSelection from './cardSelection'
import genderSelection from './genderSelection'
import ageRangeSelection from './ageRangeSelection'
import sessionStatistics from './sessionStatistics'

export default combineReducers({
    tagSelection,
    cardSelection,
    genderSelection,
    ageRangeSelection,
    sessionStatistics
})

export { State as SessionStatistics, SessionStatistic, StatisticValuesByType } from './sessionStatistics'