import { expect } from 'chai'

import reducer from './cardSelection'
import { SELECT_CARD } from '../constants'

describe('session-statistics', () => {
    describe('cardSelection reducer', () => {
        it('should be initially be unselected', () => {
            expect(reducer(undefined, {})).eql(null)
        })

        it('should handle SELECT_CARD', () => {
            const card = true
            const selectCardAction = {
                type: SELECT_CARD,
                card
            }
            expect(reducer(true, selectCardAction)).to.deep.equal(card)
        })
    })
})
