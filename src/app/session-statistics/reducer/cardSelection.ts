import { SELECT_CARD } from '../constants'

const reducer = (state = null, action) => {
    switch (action.type) {
      case SELECT_CARD:
        return action.card;
      default:
        return state;
    }
  }

export default reducer
