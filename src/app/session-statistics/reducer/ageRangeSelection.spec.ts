import { expect } from 'chai'

import reducer from './ageRangeSelection'
import { SELECT_AGE_RANGE } from '../constants'

describe('session-statistics', () => {
    describe('ageRangeSelection reducer', () => {
        const ageRangeSelection = [10,20]
        const selectAgeRangeAction = {
            type: SELECT_AGE_RANGE,
            ageRange: ageRangeSelection
        }

        it('should be initially be unselected', () => {
            expect(reducer(undefined, {})).eql(null)
        })

        it('should handle SELECT_AGE_RANGE', () => {
            expect(reducer(true, selectAgeRangeAction)).to.deep.equal(ageRangeSelection)
        })
    })
})
