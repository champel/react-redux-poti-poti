import { SELECT_TAG } from '../constants';

const reducer = (state = null, action) => {
    switch (action.type) {
      case SELECT_TAG:
        return action.tag;
      default:
        return state;
    }
  }

export default reducer
