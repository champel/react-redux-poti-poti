import { SELECT_GENDER } from '../constants'

const reducer = (state = null, action) => {
    switch (action.type) {
      case SELECT_GENDER:
        return action.gender;
      default:
        return state;
    }
  }

export default reducer
