import { expect } from 'chai'

import { fetchAll, selectAgeRange, selectCard, selectGender, selectTag } from './actions'
import { TypeKeys, SELECT_AGE_RANGE, SELECT_CARD, SELECT_GENDER, SELECT_TAG } from './constants'

import * as fixtures from './reducer/sessionStatistics.fixtures'

describe('session-statistics', () => {

    describe('actions', () => {
        it('should fetch all', () => {
            const fetchAllStatisticsAction = {
                type: TypeKeys.FETCH_ALL,
                payload: fixtures.statisticsPayload
            }
            expect(fetchAll(fixtures.statisticsPayload)).eql(fetchAllStatisticsAction)
        })

        it('should select a ageRange', () => {
            const ageRangeSelection = [10,20]
            const selectAgeRangeAction = {
                type: SELECT_AGE_RANGE,
                ageRange: ageRangeSelection
            }
            expect(selectAgeRange(ageRangeSelection)).eql(selectAgeRangeAction)
        })
        it('should select a card', () => {
            const card = true
            const selectCardAction = {
                type: SELECT_CARD,
                card
            }
            expect(selectCard(card)).eql(selectCardAction)
        })
        it('should select a gender', () => {
            const gender = 'women'
            const selectGenderAction = {
                type: SELECT_GENDER,
                gender
            }
            expect(selectGender(gender)).eql(selectGenderAction)
        })

        it('should select a tag', () => {
            const tag = 'tag1'
            const selectTagAction = {
                type: SELECT_TAG,
                tag
            }
            expect(selectTag(tag)).eql(selectTagAction)
        })
    })
})