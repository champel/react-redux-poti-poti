export const SELECT_AGE_RANGE = '@deviceStatistics/SELECT_AGE_RANGE'
export const SELECT_CARD = '@deviceStatistics/SELECT_CARD'
export const SELECT_GENDER = '@deviceStatistics/SELECT_GENDER'
export const SELECT_TAG = '@deviceStatistics/SELECT_TAG'

export enum TypeKeys {
    REQUEST = '@deviceStatistics/REQUEST',
    FETCH_ALL = '@deviceStatistics/FETCH_ALL'
}

