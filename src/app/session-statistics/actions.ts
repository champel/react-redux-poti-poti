import { SELECT_AGE_RANGE, SELECT_CARD, SELECT_GENDER, SELECT_TAG, TypeKeys } from './constants'

export const selectAgeRange = (ageRange) => ({
    type: SELECT_AGE_RANGE,
    ageRange
  })
  
export const selectCard = (card) => ({
  type: SELECT_CARD,
  card
})

export const selectGender = (gender) => ({
  type: SELECT_GENDER,
  gender
})

export const selectTag = (tag) => ({
  type: SELECT_TAG,
  tag
})

export interface Request {
    type: TypeKeys.REQUEST
}

export const request = (): Request => ({
    type: TypeKeys.REQUEST
});

export type StatisticValuesByType = { [statistic:string]: { [value:string]: number }};

export interface SessionStatistic {
    age: number,
    card: boolean,
    gender: string,
    statistics: StatisticValuesByType
}

export interface Payload {
    result: string[]
    entities: {
        sessionStatistics: { [id:string]: SessionStatistic }
    }
}

export interface FetchAll {
    type: TypeKeys.FETCH_ALL,
    payload: Payload
}

export const fetchAll = (payload: Payload): FetchAll => ({
    type: TypeKeys.FETCH_ALL,
    payload
})

export type ActionTypes =
    Request |
    FetchAll;

