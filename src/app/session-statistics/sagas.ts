import { delay } from 'redux-saga'
import { put, takeEvery } from 'redux-saga/effects'

import { TypeKeys } from './constants'
import { fetchAll } from './actions'

import { payload } from '@/fixtures/sessionStatistics'

export function* delayedFetch() {
  yield delay(2000)
  yield put(fetchAll(payload))
}

function* loader() {
  yield takeEvery(TypeKeys.REQUEST, delayedFetch);
}

export default [ loader ]