import * as React from 'react'

import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core';

import { withTagsLoaded, withAllIdsToProps, TagChips } from '../tags'
import { compose } from 'recompose';

export interface DeviceStatisticsContainerProps {
    onViewDevices
}

export interface DeviceStatisticsProps extends DeviceStatisticsContainerProps {
    tags: string[],
}


const DeviceStatistics: React.SFC<DeviceStatisticsProps> = ({tags, onViewDevices}) => (
    <>
        <Typography>Here all the tags:</Typography>
        <TagChips ids={tags}/>
        <br/>
        <Typography variant="display1">Here I would add statistics from the devices</Typography>
        <br/>
        <Button variant="outlined" color="primary" onClick={onViewDevices}>Click here to see how can be reused the devices route from another route</Button>
    </>
);

const enhance = compose(
    withTagsLoaded,
    withAllIdsToProps("tags")
)

export default enhance(DeviceStatistics) as React.ComponentClass<DeviceStatisticsContainerProps>