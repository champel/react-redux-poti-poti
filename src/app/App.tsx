import * as React from 'react'

import { Typography } from '@material-ui/core';
import { Route, withRouter } from 'react-router'

import { DefaultLayout, DefaultDrawer, DefaultToolbar } from './layout'

import { Home } from './home'
import { About } from './about'
import { DeviceStatistics } from './device-statistics'
import { SessionStatisticsPanel, SessionStatisticsSelectors } from './session-statistics'
import { RouteDevices } from './devices-route';

import Menu from './Menu'

const App = () =>
    <div style={{height: '100%', minHeight: '100%'}}>
        <Route exact path="/" component={HomePage}/>
        <Route exact path="/about" component={AboutPage}/>
        <RouteDevices root="/devices" menu={<Menu/>}/>
        <Route exact path="/sessionStatistics" component={SessionStatisticsPage}/>
        <Route exact path="/deviceStatistics" component={DeviceStatisticsPage}/>
        <RouteDevices root="/deviceStatistics/devices" menu={<Menu/>}/>
    </div>

const AboutPage = () =>
    <Layout>
        <About />
    </Layout>

const SessionStatisticsPage = () =>
    <Layout title="Session Statistics">
        <Typography variant="headline" gutterBottom>Session Statistics</Typography>
        <br/>
        <SessionStatisticsSelectors/>
        <br/>
        <SessionStatisticsPanel/>
    </Layout>

const HomePage = () => 
    <Layout toolbar={<DefaultToolbar title="Hello world!" onSearch={() => console.log('onSearch')}/>} left={<DefaultDrawer header="Home"/>}>
        <Typography variant="headline" gutterBottom>Tell us about yourself!</Typography>
        <Home />
    </Layout>

const DeviceStatisticsPage = withRouter(({history}) =>
    <Layout title="Device statistics">
        <Typography variant="headline" gutterBottom>Device Statistics</Typography>
        <DeviceStatistics onViewDevices={() => history.push("/deviceStatistics/devices")}/>
    </Layout>
)

const Layout: React.SFC<any> = ({title, children}) => <DefaultLayout title={title} menu={<Menu/>}>{children}</DefaultLayout>

export default App