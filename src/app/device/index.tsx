import DeviceDetails from './DeviceDetails'
import DeviceName from './DeviceName'

export { DeviceDetails, DeviceName }
