import * as React from 'react'

import { SFC } from 'react';
import { compose } from 'recompose'

import { withDeviceDetailsLoaded, withDeviceToProps } from '../devices'

const DeviceName: SFC<{ device: any, isLoaded: boolean}> = ({device, isLoaded}) => (
    <span>{device ? device.name : '...'}</span>
);

export interface DeviceNameProps {
    uuid: any
}

const deviceIdSelector = (_,props) => props.uuid

const enhance = compose(
    withDeviceDetailsLoaded(deviceIdSelector),
    withDeviceToProps(deviceIdSelector)
)

export default enhance(DeviceName) as React.ComponentClass<DeviceNameProps>