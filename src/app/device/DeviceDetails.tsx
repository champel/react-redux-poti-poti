import * as React from 'react'

import { SFC } from 'react';
import { compose } from 'recompose'

import { withDeviceDetailsLoaded, withDeviceToProps } from '../devices';
import { TagChips, withTagsLoaded } from '../tags';

const DeviceDetails: SFC<{device: any}> = ({device}) => (
    <div>
        {device ? <TagChips ids={device.tags}/> : '...'}
    </div>
);

export interface DeviceDetailsProps {
    uuid: any
}

const deviceIdSelector = (_,props) => props.uuid

const enhance = compose(
    withDeviceDetailsLoaded(deviceIdSelector),
    withTagsLoaded,
    withDeviceToProps(deviceIdSelector),
)

export default enhance(DeviceDetails) as React.ComponentClass<DeviceDetailsProps>
