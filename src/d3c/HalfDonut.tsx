import * as React from 'react'
import { Component } from 'react'
import * as d3 from 'd3'

import responsiveWrapper from './responsiveWrapper'
import { PieArcDatum, Arc } from 'd3';

const margin = 10
const thickness = 60

const arc = (radius: () => number) => d3.arc<PieArcDatum<any>>()
    .innerRadius(radius() - margin - thickness)
    .outerRadius(radius()  - margin);

const arcHover = (radius: () => number) => d3.arc<PieArcDatum<any>>()
    .innerRadius(radius() - margin - thickness + 10)
    .outerRadius(radius() - margin + 10);

const arcTransitioner = (arcFunc: (radius: () => number) => Arc<any, PieArcDatum<any>>, radius: () => number) => (d, i, all) => 
    d3.select(all[i])
        .transition()
        .duration(500)
        .attr('d', arcFunc(radius))

function halfPie<T>(value) {
    const angleRange = 0.5 * Math.PI;
    return d3.pie<T>()
        .sort(null)
        .value(value)
        .startAngle(angleRange * -1)
        .endAngle(angleRange);
}

export interface HalfDonutProps<T> {
    width: number
    height: number
    colors: string[]
    data: T[]
    value?: (d: T) => number
}

class HalfDonut<T> extends Component<HalfDonutProps<T>> {
   node: Element

   constructor(props){
      super(props)
      this.createHalfDonut = this.createHalfDonut.bind(this)
   }
   radius() {
      return Math.min(this.props.width, 2 * this.props.height) / 2
   }
   componentDidMount() {
      this.createHalfDonut()
   }
   componentDidUpdate() {
      this.createHalfDonut()
   }
   createHalfDonut() {
        const myValue = this.props.value ? this.props.value : (datum: T) => +datum;
        const myColors = this.props.colors ? this.props.colors : d3.scaleOrdinal(d3.schemeCategory10)
        const currentRadius = () => this.radius()

        const arcs = d3.select(this.node).selectAll("path")
            .data(halfPie(myValue)(this.props.data));

        arcs.exit().remove()
        arcs.enter()
            .append("path")
                .attr("fill", (d, i) => myColors[i])
                .attr("stroke", 'white')
                .attr("stroke-width", 2)
                .attr("d", arc(currentRadius))
                .on('mouseover', arcTransitioner(arcHover, currentRadius))
                .on('mouseout', arcTransitioner(arc, currentRadius))
        arcs
            .attr("fill", (d, i) => myColors[i] )
            .attr("d", arc(currentRadius))
    }

render() {
    const {width} = this.props

    return (
        <svg width={width} height={this.radius() + margin}>
            <g ref={node => this.node = node} transform={`translate(${width / 2},${this.radius() + margin/2 })`}/>
        </svg>)
   }
}

const ResponsiveHalfDonut = responsiveWrapper<HalfDonutProps<any>>(HalfDonut)

export { HalfDonut, ResponsiveHalfDonut }