import * as React from 'react'
import { Component, ReactElement } from 'react'

interface ResponsiveChartState {
  containerWidth: number
}

export interface ResponsiveChartStateProps {
  width: number
}

function responsiveWrapper<T extends ResponsiveChartStateProps>(ChartComponent: React.ComponentClass<T>): React.ComponentClass<T> {
  return (
    class ResponsiveChart extends Component<T, ResponsiveChartState> {
      private chartContainer: Element

      constructor(props) {
        super(props)

        this.state = {
          containerWidth: null,
        }

        this.fitParentContainer = this.fitParentContainer.bind(this)
      }

      componentDidMount() {
        this.fitParentContainer()
        window.addEventListener('resize', this.fitParentContainer)
      }

      componentWillUnmount() {
        window.removeEventListener('resize', this.fitParentContainer)
      }

      fitParentContainer() {
        const { containerWidth } = this.state
        const currentContainerWidth = this.chartContainer
          .getBoundingClientRect().width

        const shouldResize = containerWidth !== currentContainerWidth

        if (shouldResize) {
          this.setState({
            containerWidth: currentContainerWidth,
          })
        }
      }

      renderChart() {
        const parentWidth = this.state.containerWidth

        return (
          <ChartComponent {...this.props} width={this.state.containerWidth} />
        )
      }

      render() {
        const { containerWidth } = this.state
        const shouldRenderChart = containerWidth !== null

        return (
          <div
            ref={(el) => { this.chartContainer = el }}
            className="Responsive-wrapper"
          >
            {shouldRenderChart && this.renderChart()}
          </div>
        )
      }
    }
  )
}

export default responsiveWrapper