export const tag1 = {
    id: 'tag1',
    name: 'Tag 1',
    color: 'red'
}
export const tag2 = {
    id: 'tag2',
    name: 'Tag 2',
    color: 'red'
}

export const tag3 = {
    id: 'tag3',
    name: 'Tag 3',
    color: 'blue'
}
  
export const byId = {
    [tag1.id]: tag1,
    [tag2.id]: tag2,
    [tag3.id]: tag3,
}

export const allIds = [tag1.id, tag2.id, tag3.id]
  
export const payload = {
    entities: {
      tags: byId
    },
    result: allIds
}