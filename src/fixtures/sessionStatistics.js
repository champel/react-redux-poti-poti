export const statistic1 = {
    id: 'statistic1',
    card: true,
    gender: 'women',
    age: 20,
    statistics: {
        bmi: {
            underweight: 4,
            normal: 10,
            overweight: 3,
            obese: 2
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        },
        bp: {
            normal: 4,
            preHypertension: 5,
            stage1Hypertension: 7,
            stage2Hypertension: 2,
        },
        cr: {
            bellow10: 4,
            bellow20: 5,
            bellow30: 7,
            bellow40: 4,
            bellow50: 4,
            bellow60: 2,
            bellow70: 1,
        },
        vasc: {
            type1: 4,
            type2: 5,
            type3: 7,
            type4: 4,
            type5: 4,
            type6: 2,
            type7: 1,
        },
    }
  }
  
export const statistic2 = {
    id: 'statistic2',
    card: false,
    gender: 'man',
    age: 21,
    statistics: {
        bmi: {
            underweight: 7,
            normal: 3,
            overweight: 4,
            obese: 3
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        },
        bp: {
            normal: 4,
            preHypertension: 7,
            stage1Hypertension: 2,
            stage2Hypertension: 3,
        },
        cr: {
            bellow10: 1,
            bellow20: 3,
            bellow30: 5,
            bellow40: 1,
            bellow50: 7,
            bellow60: 2,
            bellow70: 4,
        },
        vasc: {
            type1: 3,
            type2: 4,
            type3: 6,
            type4: 2,
            type5: 3,
            type6: 5,
            type7: 3,
        },
    }
}
  
export const statistic3 = {
    id: 'statistic3',
    card: true,
    gender: 'man',
    age: 22,
    statistics: {
        bmi: {
            underweight: 7,
            normal: 3,
            overweight: 4,
            obese: 3
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        },
        bp: {
            normal: 8,
            preHypertension: 3,
            stage1Hypertension: 4,
            stage2Hypertension: 5,
        },
        cr: {
            bellow10: 1,
            bellow20: 3,
            bellow30: 2,
            bellow40: 1,
            bellow50: 6,
            bellow60: 7,
            bellow70: 1,
        },
        vasc: {
            type1: 1,
            type2: 4,
            type3: 7,
            type4: 5,
            type5: 3,
            type6: 5,
            type7: 2,
        },
    }
}
  
export const byId = {
    [statistic1.id]: statistic1,
    [statistic2.id]: statistic2,
    [statistic3.id]: statistic3,
}

export const allIds = [statistic1.id, statistic2.id, statistic3.id]
  
export const payload = {
    entities: {
      sessionStatistics: byId
    },
    result: allIds
  }