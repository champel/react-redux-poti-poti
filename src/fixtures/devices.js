import {tag1, tag2, tag3} from './tags'

export const device1 = {
    uuid: 'uuid:1',
    serialNumber: 'davi000001',
    name: 'Scale 01',
    status: 'connected',
    lastUpdate: '',
    tags: [tag1.id, tag2.id],
    lastData: ''
  }
  export const device2 = {
    uuid: 'uuid:2',
    serialNumber: 'davi000002',
    name: 'Scale 02',
    status: 'disconnected',
    lastUpdate: '',
    tags: [tag2.id, tag3.id],
    lastData: ''
  }
  
export const byId = {
  [device1.uuid]: device1,
  [device2.uuid]: device2,
}
export const allIds = [device1.uuid, device2.uuid]

export const payload = {
  entities: {
    devices: byId
  },
  result: allIds
}