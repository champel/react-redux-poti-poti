import reducer, { openLanguageMenu, closeLanguageMenu, OPEN_LANGUAGE_MENU, CLOSE_LANGUAGE_MENU, isLanguageMenuOpen } from '@/reducers/languageMenu'

describe('language menu', () => {
  const openLanguageMenuAction = {
    type: OPEN_LANGUAGE_MENU
  }
  const closeLanguageMenuAction = {
    type: CLOSE_LANGUAGE_MENU
  }

  describe('actions', () => {
    it('should open the language menu', () => {
      expect(openLanguageMenu()).eql(openLanguageMenuAction)
    })
    it('should close the language menu', () => {
      expect(closeLanguageMenu()).eql(closeLanguageMenuAction)
    })
  })

  describe('reducer', () => {
    it('should be initially closed', () => {
      expect(reducer(undefined, {})).eql(false)
    })

    it('should handle OPEN_LANGUAGE_MENU opening the menu', () => {
      expect(
        reducer(false, openLanguageMenuAction)
      ).to.deep.equal(true)
    })

    it('should handle CLOSE_LANGUAGE_MENU closing the menu', () => {
      expect(
        reducer(true, closeLanguageMenuAction)
      ).to.deep.equal(false)
    })
  })

  describe('selectors', () => {
    it('should inform if the language menu is open', () => {
      const languageMenu = true
      expect(isLanguageMenuOpen(languageMenu))
        .to.deep.equal(languageMenu)
    })
  })
})