import reducer, { getSessionStatisticsSelection, getSessionStatisticsByFilters, getAllTags } from '@/reducers'
import { fetchAllSessionStatistics, fetchAllTags } from '@/reducers/entities';
import { selectGender, selectCard, selectAgeRange } from '@/reducers/selections';

import { fetchAllTagsAction, allTags } from './entities/tags.fixtures'
import { statisticsPayload, statisticsByWomen, statisticsByCard, statisticsAgeRange, statisticsByAgeRange } from './entities/sessionStatistics.fixtures'
import { genderWomanSelection } from './selections/spec.fixtures'

describe('state', () => {
    it('should provide session statistics filters', () => {
        const s1 = reducer(undefined, selectGender('women'))
        expect(getSessionStatisticsSelection(s1)).to.deep.equal(genderWomanSelection)
    })
    it('should provide filtered and aggregated session statistics by gender', () => {
        const s1 = reducer(undefined, fetchAllSessionStatistics(statisticsPayload))
        const s2 = reducer(s1, selectGender('women'))
        expect(getSessionStatisticsByFilters(s2)).to.deep.equal(statisticsByWomen)
    })
    it('should provide filtered and aggregated session statistics by card', () => {
        const s1 = reducer(undefined, fetchAllSessionStatistics(statisticsPayload))
        const s2 = reducer(s1, selectCard(true))
        expect(getSessionStatisticsByFilters(s2)).to.deep.equal(statisticsByCard)
    })
    it('should provide filtered and aggregated session statistics by age range', () => {
        const s1 = reducer(undefined, fetchAllSessionStatistics(statisticsPayload))
        const s2 = reducer(s1, selectAgeRange(statisticsAgeRange))
        expect(getSessionStatisticsByFilters(s2)).to.deep.equal(statisticsByAgeRange)
    })
    it('should provide all tags', () => {
        const s1 = reducer(undefined, fetchAllTagsAction)
        expect(getAllTags(s1)).to.deep.equal(allTags)
    })

    require('./entities/spec')
    require('./selections/spec')
})
  

