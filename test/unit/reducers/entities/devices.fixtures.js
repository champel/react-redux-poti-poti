import { tag1, tag2, tag3 } from './tags.fixtures'

export const device1 = {
  uuid: 'uuid:1',
  serialNumber: 'davi000001',
  name: 'Scale 01',
  status: 'connected',
  lastUpdate: '',
  tags: [tag1.id, tag2.id],
  lastData: ''
}
export const device2 = {
  uuid: 'uuid:2',
  serialNumber: 'davi000002',
  name: 'Scale 02',
  status: 'disconnected',
  lastUpdate: '',
  tags: [tag2.id, tag3.id],
  lastData: ''
}
export const device1Details = {
  campaigns: [{
    campaign: 'campaign1',
    status: 'active'
  }, {
    campaign: 'campaign2',
    status: 'loaded'
  }],
  sessions: {
    'january': 100,
    'february': 120,
    'march': 112,
    'april': 90,
    'may': 210,
    'june': 190,
    'july': 99,
    'august': 120,
    'september': 220,
    'october': 210,
    'november': 201,
    'december': 90,
  },
  disconnections: {
    'january': 2,
    'february': 7,
    'march': 5,
    'april': 0,
    'may': 3,
    'june': 3,
    'july': 6,
    'august': 2,
    'september': 8,
    'october': 4,
    'november': 5,
    'december': 9,
  }
}
export const allDeviceIds = [device1.uuid, device2.uuid]
export const allDevicesById = {
  [device1.uuid]: device1,
  [device2.uuid]: device2 
}
export const emptyDevicesState = {allIds: [], byId: {}}
export const filledDevicesState = {
  byId: allDevicesById,
  allIds: allDeviceIds
}
export const filledDevicesStateWithDetails = {
  byId: {
    ...allDevicesById,
    [device1.uuid]: {
      ...device1,
      ...device1Details
    }
  },
  allIds: allDeviceIds
}
