import reducer, {
  getDevicesTable
} from '@/reducers/entities'

import * as fixtures from './spec.fixtures'

describe('entities', () => {
  it('should get a table of devices', () => {
    expect(getDevicesTable(fixtures.filledEntitiesState))
      .to.deep.equal(fixtures.devicesTable)
  })

  require('./tags.spec')
  require('./devices.spec')
  require('./campaigns.spec')
  require('./sessionStatistics.spec')

})