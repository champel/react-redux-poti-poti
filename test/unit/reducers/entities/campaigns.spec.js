import reducer, {
  FETCH_ALL, fetchAll,
  FETCH_DETAILS, fetchDetails,
  getById, getAllIds
} from '@/reducers/entities/campaigns'

import * as fixtures from './campaigns.fixtures'

describe('campaigns', () => {
  const campaignsPayolad = {
    entities: {
      campaigns: fixtures.allCampaignsById
    },
    result: fixtures.allCampaignIds
  } 

  const fetchAllCampaignsAction = {
    type: FETCH_ALL,
    payload: campaignsPayolad
  }

  const fetchCampaignDetailsAction = {
    type: FETCH_DETAILS,
    id: fixtures.campaign1.id,
    payload: fixtures.campaign1Details
  }

  describe('actions', () => {
    it('should fetch all', () => {
      expect(fetchAll(campaignsPayolad)).eql(fetchAllCampaignsAction)
    })
    it('should fetch details', () => {
      expect(fetchDetails(fixtures.campaign1.id, fixtures.campaign1Details)).eql(fetchCampaignDetailsAction)
    })
  })
  describe('reducer', () => {
    it('should return an empty object as initial state', () => {
      expect(reducer(undefined, {})).eql(fixtures.emptyState)
    })
    it('should handle FETCH_ALL generating a normalized state', () => {
      expect(reducer(fixtures.emptyState, fetchAllCampaignsAction)).to.deep.equal(fixtures.filledState)
    })
    it('should handle FETCH_DETAILS enriching details of an element', () => {
      expect(reducer(fixtures.filledState, fetchCampaignDetailsAction)).to.deep.equal(fixtures.filledStateWithDetails)
    })
  })
  describe('selectors', () => {
    it('should get the indexed elements', () => {
      expect(getById(fixtures.filledState)).eql(fixtures.allCampaignsById)
    })
    it('should get all the identifiers', () => {
      expect(getAllIds(fixtures.filledState)).eql(fixtures.allCampaignIds)
    })
  })
})