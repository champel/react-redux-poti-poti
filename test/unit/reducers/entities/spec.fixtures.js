import { device1, device2, filledDevicesState } from './devices.fixtures'
import { tag1, tag2, tag3, filledTagsState } from './tags.fixtures'

export const filledEntitiesState = {
    devices: filledDevicesState,
    tags: filledTagsState
}

export const devicesTable = [
    {
        ...device1,
        tags: [ tag1, tag2 ]
    },
    {
        ...device2,
        tags: [ tag2, tag3 ]
    }
]
