export const statistic1 = {
    id: 'statistic1',
    card: true,
    gender: 'women',
    age: 20,
    statistics: {
        bmi: {
            underweight: 4,
            normal: 10,
            overweight: 3,
            obese: 2
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        }
    }
}

export const statistic2 = {
    id: 'statistic2',
    card: false,
    gender: 'man',
    age: 21,
    statistics: {
        bmi: {
            underweight: 7,
            normal: 3,
            overweight: 4,
            obese: 3
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        }
    }
}

export const statistic3 = {
    id: 'statistic3',
    card: true,
    gender: 'man',
    age: 22,
    statistics: {
        bmi: {
            underweight: 7,
            normal: 3,
            overweight: 4,
            obese: 3
        },
        bf: {
            underweight: 1,
            normal: 1,
            overweight: 1,
            obese: 1
        }
    }
}

export const statisticsById = {
    [statistic1.id]: statistic1,
    [statistic2.id]: statistic2,
    [statistic3.id]: statistic3,
}
export const allStatisticIds = [statistic1.id, statistic2.id, statistic3.id]
export const filledStatisticsState = { byId: statisticsById, allIds: allStatisticIds, loaded: true, loading:false }
export const emptyStatisticsState = { byId: {}, allIds: [], loaded: false, loading: false }

export const statisticsPayload = {
    entities: {
      sessionStatistics: statisticsById
    },
    result: allStatisticIds
}

export const statisticsWithoutFilters = {
    bmi: {
        normal: 16,
        obese: 8,
        overweight: 11,
        underweight: 18,
    },
    bf: {
        normal: 3,
        obese: 3,
        overweight: 3,
        underweight: 3,
    },
}

export const statisticsByWomen = {
    bmi: {
        normal: 10,
        obese: 2,
        overweight: 3,
        underweight: 4,
      },
    bf: {
        normal: 1,
        obese: 1,
        overweight: 1,
        underweight: 1,
    },
}

export const statisticsByCard = {
    bmi: {
        normal: 13,
        obese: 5,
        overweight: 7,
        underweight: 11,
      },
    bf: {
        normal: 2,
        obese: 2,
        overweight: 2,
        underweight: 2,
    },
}
export const statisticsAgeRange = [21,22]

export const statisticsByAgeRange = {
    bmi: {
        normal: 6,
        obese: 6,
        overweight: 8,
        underweight: 14,
      },
    bf: {
        normal: 2,
        obese: 2,
        overweight: 2,
        underweight: 2,
    },
}