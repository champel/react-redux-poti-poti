import { device1, device2 } from './devices.fixtures'

export const campaign1 = {
    id: 'campaign1',
    name: 'Campaign 1'
}
export const campaign2 = {
    id: 'campaign2',
    name: 'Campaign 2'
}

export const campaign1Details = {
    files: {
      logo: 'logo',
      banners: [
        'banner1',
        'bannee2'
      ],
      video: undefined
    },
    devices: [{
      device: device1.uuid,
      status: 'active'
    },{
      device: device2.uuid,
      status: 'active'
    }]
}
export const allCampaignIds = [campaign1.id, campaign2.id]
export const allCampaignsById = {
    [campaign1.id]: campaign1,
    [campaign2.id]: campaign2 
}

export const emptyState = {allIds: [], byId: {}}

export const filledState = {
    byId: allCampaignsById,
    allIds: allCampaignIds
}

export const filledStateWithDetails = {
    byId: {
        ...allCampaignsById,
        [campaign1.id]: {
        ...campaign1,
        ...campaign1Details
        }
    },
    allIds: allCampaignIds
}
  
  