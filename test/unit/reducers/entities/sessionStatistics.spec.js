import reducer, {
    FETCH_ALL, fetchAll,
    getByFilters
} from '@/reducers/entities/sessionStatistics'

import * as fixtures from './sessionStatistics.fixtures'

describe('statistics', () => {
    const fetchAllStatisticsAction = {
        type: FETCH_ALL,
        payload: fixtures.statisticsPayload
    }

    describe('actions', () => {
        it('should fetch all', () => {
            expect(fetchAll(fixtures.statisticsPayload)).eql(fetchAllStatisticsAction)
        })
    })
    describe('reducer', () => {
        it('should return an empty object as initial state', () => {
            expect(reducer(undefined, {})).eql(fixtures.emptyStatisticsState)
        })
        it('should handle FETCH_ALL filling the state', () => {
            expect(reducer(fixtures.emptyStatisticsState, fetchAllStatisticsAction)).to.deep.equal(fixtures.filledStatisticsState)
        })
    })
    describe('selectors', () => {
        it('should get the indexed elements', () => {
            expect(getByFilters(fixtures.filledStatisticsState,{})).eql(fixtures.statisticsWithoutFilters)
        })
        it('should get the indexed elements', () => {
            expect(getByFilters(fixtures.filledStatisticsState,{card: true, gender:'women', age: 20})).eql(fixtures.statisticsByWomen)
        })
    })
})