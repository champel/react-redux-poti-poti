import reducer, {
  FETCH_ALL, fetchAll,
  FETCH_DETAILS, fetchDetails,
  getById, getAllIds
} from '@/reducers/entities/devices'

import * as fixtures from './devices.fixtures'

describe('devices', () => {
  const devicesPayolad = {
    entities: {
      devices: fixtures.allDevicesById
    },
    result: fixtures.allDeviceIds
  } 

  const fetchAllDevicesAction = {
    type: FETCH_ALL,
    payload: devicesPayolad
  }

  const fetchDeviceDetailsAction = {
    type: FETCH_DETAILS,
    id: fixtures.device1.uuid,
    payload: fixtures.device1Details
  }

  describe('actions', () => {
    it('should fetch all', () => {
      expect(fetchAll(devicesPayolad)).eql(fetchAllDevicesAction)
    })
    it('should fetch details', () => {
      expect(fetchDetails(fixtures.device1.uuid,fixtures.device1Details)).eql(fetchDeviceDetailsAction)
    })
  })
  describe('reducer', () => {
    it('should return an empty object as initial state', () => {
      expect(reducer(undefined, {})).eql(fixtures.emptyDevicesState)
    })
    it('should handle FETCH_ALL generating a normalized state', () => {
      expect(reducer(fixtures.emptyDevicesState, fetchAllDevicesAction)).to.deep.equal(fixtures.filledDevicesState)
    })
    it('should handle FETCH_DETAILS enriching details of an element', () => {
      expect(reducer(fixtures.filledDevicesState, fetchDeviceDetailsAction)).to.deep.equal(fixtures.filledDevicesStateWithDetails)
    })
  })
  describe('selectors', () => {
    it('should get the indexed elements', () => {
      expect(getById(fixtures.filledDevicesState)).eql(fixtures.allDevicesById)
    })
    it('should get all the identifiers', () => {
      expect(getAllIds(fixtures.filledDevicesState)).eql(fixtures.allDeviceIds)
    })
  })
})