import reducer, { toggleMenu, TOGGLE_MENU, isMenuOpen } from '@/reducers/menu'

describe('menu', () => {
  describe('actions', () => {
    it('should toggle menu', () => {
      const expectedAction = {
        type: TOGGLE_MENU
      }
      expect(toggleMenu()).eql(expectedAction)
    })
  })

  describe('reducer', () => {
    it('should be initially closed', () => {
      expect(reducer(undefined, {})).eql(false)
    })

    it('should handle TOGGLE_MENU closing when open', () => {
      expect(
        reducer(true, {
          type: TOGGLE_MENU
        })
      ).to.deep.equal(false)
    })

    it('should handle TOGGLE_MENU opening when closed', () => {
      expect(
        reducer(false, {
          type: TOGGLE_MENU
        })
      ).to.deep.equal(true)
    })
  })

  describe('selectors', () => {
    it('should inform if menu is open', () => {
      const menu = true
      expect(isMenuOpen(menu))
        .to.deep.equal(menu)
    })
  })
})