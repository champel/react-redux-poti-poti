export const genderWomanSelection = {
  gender: 'women',
  card: null,
  ageRange: null
}
  
export const sessionStatisticsEmptySelection = {
  gender: null,
  card: null,
  ageRange: null
}  