import reducer, {
  selectGender, getSessionStatisticsSelection
} from '@/reducers/selections'

import * as fixtures from './spec.fixtures'

describe('selections', () => {
  it('return null statistics selections at the beggining', () => {
    const state = reducer(undefined, {})
    expect(getSessionStatisticsSelection(state))
      .to.deep.equal(fixtures.sessionStatisticsEmptySelection)
  })
  it('should include the gender selection into the session statistics selection', () => {
    const state = reducer(undefined, selectGender('women'))
    expect(getSessionStatisticsSelection(state))
      .to.deep.equal(fixtures.genderWomanSelection)
  })
  require('./genderSelection.spec')
  require('./cardSelection.spec')
  require('./ageRangeSelection.spec')
  require('./tagSelection.spec')
})