module.exports = {
    moduleFileExtensions: [
      "ts",
      "tsx",
      "js"
    ],
    moduleNameMapper: {
      "@/(.*)$": "<rootDir>/src/$1"
    },
    transform: {
      "^.+\\.jsx?$": "babel-jest",
      "^.+\\.(ts|tsx)$": "ts-jest"
    },
    globals: {
      "ts-jest": {
        "tsConfigFile": "tsconfig.json"
      }
    },
    testMatch: [
      "**/__tests__/**/*.js?(x)",
      "**/?(*.)+(spec|test).js?(x)",
      "**/__tests__/**/*.ts?(x)",
      "**/?(*.)+(spec|test).ts?(x)"
    ],
    roots: [
      "./src"
    ],
    setupTestFrameworkScriptFile: "<rootDir>/enzyme.js",
    
}