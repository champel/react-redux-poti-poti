const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader')
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    app: ['babel-polyfill', './src/index.tsx']
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    historyApiFallback: true
  },
  module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        },       
        {
            test: /\.tsx?$/,
            loader: 'awesome-typescript-loader',
            options: {
                "useBabel": true,
                "babelOptions": {
                    "babelrc": false, /* Important line */
                    "presets": [
                        ["@babel/preset-env", { "modules": false }]
                    ]
                },
                "babelCore": "@babel/core", // needed for Babel v7
                reportFiles: [
                    "src/**/*.{ts,tsx}"
                ]
            }
        },
        {
            test: /\.css$/,
            use: [
                'style-loader', // creates style nodes from JS strings
                'css-loader' // translates CSS into CommonJS
            ]
        },
        {
            test: /\.scss$/,
            use: [
                "style-loader", // creates style nodes from JS strings
                "css-loader", // translates CSS into CommonJS
                {
                    loader: "sass-loader", // compiles Sass to CSS
                    options: {
                        // needed to read material-components-web/material-components-web from ./src/index.scss
//                        includePaths: [path.resolve(__dirname, './node_modules')]
                    }
                }
            ]
        },
        {
            test: /\.(ttf|eot|svg|gif|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
                loader: 'file-loader'
            }]        
        }
    ]
},
  plugins: [
    new HardSourceWebpackPlugin(),
    new CleanWebpackPlugin(['dist']),
    new CopyWebpackPlugin([ { from: 'locales', to: 'locales' } ], {}),
    new HtmlWebpackPlugin({
        title: 'Output Management',
        template: "./src/index.html"
    }),
    new CheckerPlugin(),
//    new BundleAnalyzerPlugin() // Analyzes the size of the bundles
  ],
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    plugins: [
        new TsConfigPathsPlugin(/* { configFileName, compiler } */)
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: {
    splitChunks: {
        chunks: 'all'
    }
  }
};