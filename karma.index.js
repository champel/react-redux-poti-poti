import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// See: https://webpack.js.org/guides/dependency-management/#require-context
function importAll (r) {
    r.keys().forEach(r);
  }
  
// To be able to test function generators
require("babel-polyfill");

// Configure Enzyme
Enzyme.configure({ adapter: new Adapter() });

// Require files explicitily (to ensure order)
require('./src/app/hello/karma.index')

// Require all spec files
importAll(require.context('./src', true, /spec$/))